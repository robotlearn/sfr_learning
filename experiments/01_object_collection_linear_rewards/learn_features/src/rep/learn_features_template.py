#!/usr/bin/env python
##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
import exputils as eu
import rlexp
import torch

config = eu.AttrDict(
        seed=637264278 + <repetition_id>,

        n_feature_dim=<n_feature_dim>,

        n_iter=<n_iter>,

        dataset=eu.AttrDict(
            zero_reward_transitions_retention_rate=<zero_reward_transitions_retention_rate>,
            observation_type='<observation_type>'
        ),

        dataloader=eu.AttrDict(
            batch_size=<batch_size>,
            shuffle=True
        ),

        w_init_sigma = <w_init_sigma>,

        model=eu.AttrDict(
            <feature_model>
        ),

        optimizer=eu.AttrDict(
            cls=torch.optim.Adam,
            lr=<lr>,
        ),

        loss_function=eu.AttrDict(
            cls=torch.nn.MSELoss,
        ),

        reward_weight_normalization='<normalization>'
)

config.dataset.experiment_directory = '../../../../experiments/'

rlexp.agent.project.continuous_sf_01.train_features(
    <source_exp_id>,
    <source_rep>,
    config=config
)