#!/usr/bin/env python
##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
import exputils as eu
import rlexp
from train_linear_model import train_linear_model
import exputils.data.logging as log
from repetition_config import config


def log_transitions(log, env, agent, step, episode, step_per_episode, transition, exp_info):

    # only record transitions from the QL agents to be used to learn the features
    if isinstance(agent, (rlexp.agent.project.xi_learning_01.QLearningNumpy, rlexp.agent.project.xi_learning_01.DNNQLearning)):

        # only record the first 20 tasks (i.e. phases)
        if exp_info.phase < 20:

            log.add_value('transitions', transition)
            log.add_value('phase_per_transition', exp_info.phase)


# train linear weights in case the DNNSFQLearning agent needs them
if hasattr(config.agent, 'is_set_initial_reward_weights') and config.agent.is_set_initial_reward_weights:

    # set seed from config
    eu.misc.seed(config)

    for rfunc_idx, rfunc in enumerate(config.reward_functions):

        w, log_mean_error = train_linear_model(
            rfunc,
            n_iterations=10000,
            learn_rate=1.0
        )
        config.reward_functions[rfunc_idx] = (rfunc, dict(reward_weight_vector=w))

        # log the error from the training of w
        for err in log_mean_error:
            log.add_value('linear_model_error_per_iteration_for_task_{}'.format(rfunc_idx), err)

    log.save()

# run experiment resuming from a potential previous run
rlexp.exp.continual_rf_transfer.run_training(
    resume_from_log=True,
    save_log_automatically=True,
    config=config,
    log_functions=log_transitions
)