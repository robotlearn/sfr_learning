##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
import exputils as eu
import numpy as np

def train_linear_model(reward_function, config=None, **kwargs):

    default_config = eu.AttrDict(
        # possible features:
        features = np.array([
            [0, 0, 0, 0, 0],
            [1, 0, 1, 0, 0],
            [1, 0, 0, 1, 0],
            [0, 1, 1, 0, 0],
            [0, 1, 0, 1, 0],
            [0, 0, 0, 0, 1],
        ]),

        n_iterations = 10000,

        learn_rate = 1.0,

        reduction = 'mean', # 'mean' or 'sum'
    )

    config = eu.combine_dicts(kwargs, config, default_config)

    if config.reduction not in ['mean', 'sum']:
        raise ValueError('Unknown value for reduction configuration: {!r}! Allowed values: \'mean\', \'sum\'.'.format(config.reduction))

    if config.reduction == 'mean':
        reduction_func = np.mean
    else:
        reduction_func = np.sum

    log_mean_error = np.empty(config.n_iterations)

    # random initialization of weight
    w = np.random.randn(config.features.shape[1]) * 0.01

    # print(r_func)
    reward_per_feature = [reward_function(f) for f in config.features]

    for iter in range(config.n_iterations):

        expected_reward_per_feature = np.matmul(config.features, w)

        diff_per_feature = reward_per_feature - expected_reward_per_feature

        delta_w_per_feature = np.transpose([diff_per_feature]) * config.features

        reduced_delta_w = reduction_func(delta_w_per_feature, axis=0)

        w += config.learn_rate * reduced_delta_w

        # logging
        log_mean_error[iter] = np.mean(np.abs(diff_per_feature))

    return w, log_mean_error


def non_linear_reward_func(feature, object_features, reward_per_object_feature):

    # make sure features are numpy arrays
    feature = np.array(feature)

    # check if one of the features in the description is the same as the current feature
    is_match = np.apply_along_axis(
        np.all,
        1,
        feature[:-1] == object_features)

    # if yes, use its reward
    if np.any(is_match):
        reward = reward_per_object_feature[is_match][0]
    else:
        reward = 0.0

    # give a reward of 1 for reaching the goal state
    reward += feature[-1]

    return reward


def test_train_linear_model_on_non_linear_reward_function():

    n_tasks = 1

    possible_object_features = np.array([
        [1, 0, 1, 0],
        [1, 0, 0, 1],
        [0, 1, 1, 0],
        [0, 1, 0, 1],
    ])

    # set seed so that the sampling of the tasks is deterministic
    eu.misc.seed(5)

    # random generator of weights for each feature
    n_features = possible_object_features.shape[0]
    feature_combination_rewards = np.random.rand(n_tasks, n_features) * 2.0 - 1.0

    non_linear_reward_functions = []
    for task_idx in range(n_tasks):
        obj_rewards = feature_combination_rewards[task_idx, :]
        r_func = lambda feature, obj_features=possible_object_features, obj_rewards=obj_rewards: non_linear_reward_func(feature, obj_features, obj_rewards)
        non_linear_reward_functions.append(r_func)

    w, log_mean_error = train_linear_model(non_linear_reward_functions[0])

    print('w: {}'.format(w))
    print('first: {}'.format(log_mean_error[0]))
    print('last: {}'.format(log_mean_error[-1]))


def test_train_linear_model_on_linear_reward_function():

    n_tasks = 1
    feature_dim = 4

    # set seed so that the sampling of the tasks is deterministic
    eu.misc.seed(1)
    # sample weights between [-1, 1]
    reward_weights_per_task = np.random.rand(n_tasks, feature_dim) * 2 - 1
    # add weight of 1 for the feature of ending in the goal state
    reward_weights_per_task = np.hstack((reward_weights_per_task, np.ones((n_tasks, 1))))

    reward_functions = []
    for task_idx in range(n_tasks):
        reward_weights = reward_weights_per_task[task_idx, :]
        reward_func = lambda feature, w=reward_weights: np.sum(w * np.array(feature))
        reward_func_descr = dict(reward_weight_vector=reward_weights)
        reward_functions.append((reward_func, reward_func_descr))

    w, log_mean_error = train_linear_model(reward_functions[0][0])

    print('w: {}'.format(w))
    print('first: {}'.format(log_mean_error[0]))
    print('last: {}'.format(log_mean_error[-1]))


def search_best_learn_rate_for_non_linear_reward_functions(config=None, **kwargs):

    default_config = eu.AttrDict(
        n_reward_functions = 50,
        n_iterations = 100000,
        learn_rates = [0.0001, 0.0005, 0.001, 0.005, 0.01, 0.05],
        possible_object_features = np.array([
            [1, 0, 1, 0],
            [1, 0, 0, 1],
            [0, 1, 1, 0],
            [0, 1, 0, 1],
        ]),
    )
    config = eu.combine_dicts(kwargs, config, default_config)

    eu.misc.seed(2423)

    # generate reward functions

    n_tasks = config.n_reward_functions
    possible_object_features = config.possible_object_features

    # random generator of weights for each feature
    n_features = possible_object_features.shape[0]
    feature_combination_rewards = np.random.rand(n_tasks, n_features) * 2.0 - 1.0

    non_linear_reward_functions = []
    for task_idx in range(n_tasks):
        obj_rewards = feature_combination_rewards[task_idx, :]
        r_func = lambda feature, obj_features=possible_object_features, obj_rewards=obj_rewards: non_linear_reward_func(feature, obj_features,
                                                                                                                        obj_rewards)
        non_linear_reward_functions.append(r_func)

    final_error_per_lr_and_rfunc = np.full((len(config.learn_rates), config.n_reward_functions), np.nan)

    # train weights for each
    for r_func_idx in range(config.n_reward_functions):

        for lr_idx, lr in enumerate(config.learn_rates):

            _, log_mean_error = train_linear_model(
                non_linear_reward_functions[r_func_idx],
                n_iterations=config.n_iterations,
                learn_rate=lr
            )

            final_error_per_lr_and_rfunc[lr_idx, r_func_idx] = log_mean_error[-1]

    # find best learn rate
    max_lr_idx = np.argmin(np.mean(final_error_per_lr_and_rfunc, axis=1))

    return config.learn_rates[max_lr_idx], final_error_per_lr_and_rfunc



if __name__ == '__main__':
    # print('LINEAR REWARD FUNCTION')
    # test_train_linear_model_on_linear_reward_function()

    # print('\nNON-LINEAR REWARD FUNCTION')
    # test_train_linear_model_on_non_linear_reward_function()

    # identify best learn rate
    learn_rates = [0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1.0, 1.5, 2.0, 3.0]
    best_lr, final_error_per_lr_and_rfunc = search_best_learn_rate_for_non_linear_reward_functions(
        learn_rates = learn_rates,
        n_reward_functions = 50,
        n_iterations = 10000,
    )

    print('Mean error (n={}) per learn rate:'.format(final_error_per_lr_and_rfunc.shape[1]))
    for lr_idx, lr in enumerate(learn_rates):
        mean_error = np.mean(final_error_per_lr_and_rfunc[lr_idx,:])
        print('{}: {}'.format(lr, mean_error))

    print('\nBest learn rate: {}'.format(best_lr))

