##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
import numpy as np
import exputils as eu
import rlexp
import rlexp.agent.project.xi_learning_01 as agents
import rlexp.utils as utils
import gym
import torch
from reward_function_sampling import sample_reward_functions

config = eu.AttrDict(
    seed = 4568745 + <repetition_id>,

    agent = eu.AttrDict(
        cls=agents.<agent>
    ),

    env = eu.AttrDict(
        cls=rlexp.env.project.xi_learning_01.Barreto2018TwoDObjectCollection,

        objects = np.array([  # each object has a [x_pos, y_pos, radius]
            [0.08, 0.56, 0.04],
            [0.08, 0.92, 0.04],
            [0.20, 0.50, 0.04],
            [0.44, 0.56, 0.04],
            [0.44, 0.92, 0.04],
            [0.50, 0.20, 0.04],
            [0.50, 0.80, 0.04],
            [0.56, 0.08, 0.04],
            [0.56, 0.44, 0.04],
            [0.80, 0.50, 0.04],
            [0.92, 0.08, 0.04],
            [0.92, 0.44, 0.04],
        ]),

        # circle - white    [1, 0, 1, 0]
        # circle - black    [1, 0, 0, 1]
        # box - white       [0, 1, 1, 0]
        # box - black       [0, 1, 0, 1]
        object_features = np.array([
            [1, 0, 0, 1],
            [0, 1, 1, 0],
            [0, 1, 0, 1],
            [0, 1, 0, 1],
            [1, 0, 1, 0],
            [1, 0, 0, 1],
            [1, 0, 1, 0],
            [1, 0, 1, 0],
            [1, 0, 0, 1],
            [0, 1, 1, 0],
            [0, 1, 0, 1],
            [0, 1, 1, 0],
        ])
    ),

    n_tasks = 300,
    n_max_steps_per_phase = 20000,
)

################
# agent configuration depends on the used agent (config.agent_class)

# set general agent config
config.agent = eu.combine_dicts(config.agent, dict(<agent_config>), config.agent.cls.default_config())

if config.agent.cls == agents.QLearningNumpy:
    config.agent.epsilon = <epsilon>
    config.agent.alpha = <q_lr>

if config.agent.cls == agents.Barreto2018SFQL:
    config.agent.epsilon = <epsilon>
    config.agent.alpha_w = <r_lr>
    config.agent.alpha = <psi_lr>

if config.agent.cls == agents.DiscreteMFXi:
    config.agent.epsilon = <epsilon>
    config.agent.alpha = <xi_lr>
    config.agent.reward_approx = eu.combine_dicts(dict(<r_config>), config.agent.reward_approx)
    config.agent.reward_approx.optimizer.lr = <r_lr>

if config.agent.cls == agents.DiscreteMBXi:
    config.agent.epsilon = <epsilon>
    config.agent.alpha = <xi_lr>
    config.agent.onestep_sf_model = eu.combine_dicts(dict(<sf_model_config>), config.agent.onestep_sf_model)
    config.agent.onestep_sf_model.optimizer.lr = <sf_model_lr>
    config.agent.reward_approx = eu.combine_dicts(dict(<r_config>), config.agent.reward_approx)
    config.agent.reward_approx.optimizer.lr = <r_lr>

##############################################################
# sample random tasks, i.e. random non-linear reward functions

config.reward_functions = sample_reward_functions(
    n_reward_functions=config.n_tasks,
    linear_model_error_range=<linear_reward_model_error_bin>
)
