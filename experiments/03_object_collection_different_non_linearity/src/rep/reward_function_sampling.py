##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
import numpy as np


def reward_func(feature, object_features, reward_per_object_feature):
    # make sure features are numpy arrays
    feature = np.array(feature)

    # check if one of the features in the description is the same as the current feature
    is_match = np.apply_along_axis(
        np.all,
        1,
        feature == object_features)

    # if yes, use its reward
    if np.any(is_match):
        reward = reward_per_object_feature[is_match][0]
    else:
        reward = 0.0

    return reward


def sample_reward_functions(n_reward_functions, linear_model_error_range):

    n_total_samples = 1000000

    feature_mat = np.array(
        [[0, 0, 0, 0, 0],
         [0, 1, 0, 1, 0],
         [0, 1, 1, 0, 0],
         [1, 0, 0, 1, 0],
         [1, 0, 1, 0, 0],
         [0, 0, 0, 0, 1]])

    feature_mat_pinv = np.linalg.pinv(feature_mat)

    # sample the rewards for each possible feature combination
    r_samples = np.random.rand(n_total_samples, feature_mat.shape[0]) * 2 - 1
    r_samples[:, 0] = 0.0
    r_samples[:, -1] = 1.0

    # calculate the best weights and their errors
    weights = np.empty((n_total_samples, feature_mat.shape[1]))
    errors = np.empty(n_total_samples)
    for sample_idx in range(r_samples.shape[0]):
        weights[sample_idx] = feature_mat_pinv @ r_samples[sample_idx, :]
        errors[sample_idx] = np.linalg.norm(r_samples[sample_idx, :] - feature_mat @ weights[sample_idx])

    # select the needed weights
    from_border = linear_model_error_range[0]
    to_border = linear_model_error_range[1]

    possible_r = r_samples[np.logical_and(errors >= from_border, errors < to_border)]
    possible_w = weights[np.logical_and(errors >= from_border, errors < to_border)]

    selected_rewards = possible_r[:n_reward_functions]
    selected_weights = possible_w[:n_reward_functions]

    reward_functions = []
    for r_func_idx in range(n_reward_functions):
        r_func = lambda feature, obj_features=feature_mat, obj_rewards=selected_rewards[r_func_idx, :]: reward_func(feature, obj_features, obj_rewards)
        reward_functions.append((r_func, dict(reward_weight_vector=selected_weights[r_func_idx, :])))

    return reward_functions