#!/usr/bin/env python
##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
import exputils as eu
import rlexp
import exputils.data.logging as log
from repetition_config import config

# run experiment resuming from a potential previous run
rlexp.exp.continual_rf_transfer.run_training(
    resume_from_log=True,
    save_log_automatically=True,
    config=config)