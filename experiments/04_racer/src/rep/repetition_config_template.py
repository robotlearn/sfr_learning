##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##

import numpy as np
import exputils as eu
import rlexp
import rlexp.agent.project.continuous_sf_01 as agents
from rlexp.env.project.continuous_sf_01.twod_infinite_track_racer_env import sample_component_independent_rfunc
import rlexp.utils as utils
import gym
import torch


##############################
# general config

config = eu.AttrDict(
    seed = 8434 + <repetition_id>,

    agent = eu.AttrDict(
        cls = agents.<agent>,
        gamma = <gamma>,
        epsilon = <epsilon>,
        features = eu.AttrDict(
            cls=agents.<feature_type>
        ),
    ),

    env = eu.AttrDict(
        cls = rlexp.env.project.continuous_sf_01.RBFStateTwoDInfiniteTrackRacerEnv,
    ),

    n_max_episodes_per_phase = 1000,
    n_max_steps_per_epsiode = 200,
    n_reward_functions = <n_tasks>,
)

################
# agent configuration depends on the used agent (config.agent_class)

# set general agent config
config.agent = eu.combine_dicts(config.agent, dict(<agent_config>), config.agent.cls.default_config())

if config.agent.cls == agents.DNNQLearning:
    config.agent.q_func = eu.combine_dicts(dict(<q_config>), config.agent.q_func)
    config.agent.q_func.optimizer.lr = <lr>

if config.agent.cls == agents.Barreto2018SFQLPyTorch:
    config.agent.alpha = <lr>
    config.agent.alpha_w = <r_lr>
    config.agent.psi_func = eu.combine_dicts(dict(<psi_config>), config.agent.psi_func)
    config.agent.psi_func.optimizer.lr = <lr>

if config.agent.cls == agents.Barreto2018SFQLPyTorchFeature:
    config.agent.alpha = <lr>
    config.agent.alpha_w = <r_lr>
    config.agent.psi_func = eu.combine_dicts(dict(<psi_config>), config.agent.psi_func)
    config.agent.psi_func.optimizer.lr = <lr>

if config.agent.cls == agents.OneDDiscretizedContinuousPhiMFXi:
    config.agent.xi_func = eu.combine_dicts(dict(<xi_config>), config.agent.xi_func)
    config.agent.xi_func.optimizer.lr = <lr>

if config.agent.cls == agents.OneDDiscretizedContinuousPhiMFXiFeature:
    config.agent.xi_func = eu.combine_dicts(dict(<xi_config>), config.agent.xi_func)
    config.agent.xi_func.optimizer.lr = <lr>
    config.agent.reward_approx.alpha_w = <r_lr>

# provide the path to the feature model if one should be used
# this depends on the number of features <n_feature_dims> which defines experiment id used for computing the features
if '<feature_type>' == 'ModeledFeatures':
    # the learned features have experiment ids with 4xx and 8xx where x is the same repetition as the current one
    src_feature_exp_id = <n_feature_dims> * 100 + <repetition_id>
    config.agent.features.feature_model_path = '../../../learn_features/experiments/{}/{}/data/final_model.dill'.format(
        eu.EXPERIMENT_DIRECTORY_TEMPLATE.format(src_feature_exp_id),
        eu.REPETITION_DIRECTORY_TEMPLATE.format(0), # always use repetition 0 from learned features
    )

##############################################################
# reward functions

# seed to make it random sampling repeatable
eu.misc.seed(config)

# get number of markers in env
env = eu.misc.create_object_from_config(config.env)
n_markers_in_env = env.config.markers.shape[0]

config.reward_functions = []
for rfunc_idx in range(config.n_reward_functions):

    # sample the reward function
    rf_handle, rf_str, rf_component_handles = sample_component_independent_rfunc(n_markers_in_env)

    config.reward_functions.append((rf_handle,{
            'component_per_feature': rf_component_handles,
        })
    )


# remove the first 10 tasks if learned features are used
# as these tasks were used to collect the data to learn the features
if '<feature_type>' in ['ModeledFeatures', 'LinearFeatures']:
    config.reward_functions = config.reward_functions[10:]