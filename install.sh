#!/bin/bash
##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##

echo "Install rlexp package in developer mode ..."
pip install -e ./src/rlexp

echo "Install and setup jupyter notebook extensions ..."
pip install jupyter_contrib_nbextensions
jupyter contrib nbextension install --user
jupyter nbextension enable --py --sys-prefix qgrid

echo "Finished"