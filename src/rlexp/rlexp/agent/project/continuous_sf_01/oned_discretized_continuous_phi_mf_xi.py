##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
import numpy as np
import exputils as eu
import warnings
from rlexp.agent.project.continuous_sf_01 import ContinuousFeatureLinearComponentRewardFuncApproxBase
import torch
import rlexp
import exputils.data.logging as log


class OneDDiscretizedContinuousPhiMFXi(ContinuousFeatureLinearComponentRewardFuncApproxBase):
    """
    Reward functions that are a linear combination of sub-function:
        R(phi) = r_1(phi_1) + r_2(phi_2) + r_3(phi_3) + ... + r_n(phi_n)
    """

    @staticmethod
    def default_config():
        dc = eu.AttrDict(
            epsilon=0.15,  # can also be a dict that defines the function to get epsilon
            gamma=0.95,

            use_gpi = True,

            xi_func=eu.AttrDict(

                n_bins_per_feature=11,

                model=eu.AttrDict(
                    cls=rlexp.approximation.ActionSeparatedLinearNN,
                ),

                optimizer=eu.AttrDict(
                    cls=torch.optim.SGD,
                    lr=0.01,
                ),

                loss=eu.AttrDict(
                    cls=torch.nn.MSELoss,
                ),

                gradient_clipping_value=1.0,

                init_mode='previous',  # 'random' or 'previous': random crea

                feature_encoding_function = lambda distances, bin_length: (distances <= bin_length/2.0).astype(np.float)
            ),

            recalc_rewards_every_nth_step = None,
            is_set_initial_reward_weights = False,
        )

        # get default config from base class
        dc = eu.combine_dicts(
            dc,
            ContinuousFeatureLinearComponentRewardFuncApproxBase.default_config())

        return dc


    def __init__(self, env, config=None, **kwargs):
        config = eu.combine_dicts(kwargs, config, self.default_config())
        super().__init__(env, config=config)

        self.epsilon = self.config.epsilon

        self.xi_func_per_reward_func = []
        self.xi_func_loss_func = eu.misc.create_object_from_config(self.config.xi_func.loss)

        self.c = None

        self.xi_output_dimensions = [self.config.xi_func.n_bins_per_feature] * self.n_feature_dim
        self.xi_n_out = np.sum(self.xi_output_dimensions)
        self.ranges_per_feature_dim = []
        self.medians_per_feature_dim = []
        self.bin_length_per_feature_dim = []
        for f_dim in range(self.n_feature_dim):
            bin_length = (self.feature_space.high[f_dim] - self.feature_space.low[f_dim]) / (self.config.xi_func.n_bins_per_feature - 1)
            self.bin_length_per_feature_dim.append(bin_length)

            ranges = np.linspace(
                self.feature_space.low[f_dim] - bin_length / 2,
                self.feature_space.high[f_dim] + bin_length / 2,
                self.config.xi_func.n_bins_per_feature + 1
            )
            self.ranges_per_feature_dim.append(ranges)

            medians = np.linspace(
                self.feature_space.low[f_dim],
                self.feature_space.high[f_dim],
                self.config.xi_func.n_bins_per_feature
            )
            self.medians_per_feature_dim.append(medians)

        self.reward_per_feature_per_reward_func = []


    def get_feature_idxs(self, feature):
        """Identifies the index in the xi function of the value for the given feature."""

        range_idx_per_dim = np.empty(self.n_feature_dim, dtype=int)

        for f_dim in range(self.n_feature_dim):
            feature_val = feature[f_dim]
            range_idx_per_dim[f_dim] = self.get_feature_idx_per_dim(f_dim, feature_val)

        return range_idx_per_dim


    def get_feature_idx_per_dim(self, dim, feature_val):

        ranges = self.ranges_per_feature_dim[dim]

        if feature_val < ranges[0] or feature_val >= ranges[-1]:
            raise ValueError('The given feature is not in its defined range!')

        range_ind = np.logical_and(feature_val >= ranges[:-1], feature_val < ranges[1:])
        offset = dim * self.config.xi_func.n_bins_per_feature
        return offset + int(np.where(range_ind)[0])


    def get_feature_encoding(self, feature):

        feature_encoding = torch.empty(self.xi_n_out, dtype=torch.float32)

        for f_dim in range(len(feature)):

            feature_val = feature[f_dim]

            cur_bin_medians = self.medians_per_feature_dim[f_dim]

            distances = np.abs(cur_bin_medians - feature_val)

            encoding_start_idx = f_dim * self.config.xi_func.n_bins_per_feature
            encoding_end_idx = encoding_start_idx + self.config.xi_func.n_bins_per_feature

            feature_encoding[encoding_start_idx:encoding_end_idx] = torch.tensor(self.config.xi_func.feature_encoding_function(
                distances,
                self.bin_length_per_feature_dim[f_dim]
            ))

        return feature_encoding


    def add_reward_function(self, reward_function, reward_func_descr=None):

        new_rfunc_idx = super().add_reward_function(reward_function, reward_func_descr=reward_func_descr)

        # # set up initial psi-function for the new reward function
        # if not self.z_per_reward_func:
        #     if self.config.z_init is None:
        #         z = np.random.randn(self.action_space.n, self.observation_space.shape[0], self.config.xi_func.n_bins_per_feature*self.n_feature_dim) * self.config.z_init_std
        #     else:
        #         z = self.config.z_init.copy()
        # else:
        #     z = self.z_per_reward_func[self.active_reward_func_idx].copy()
        # self.z_per_reward_func.append(z)

        # # set up initial xi-function for the new reward function
        new_xi_func = eu.AttrDict()

        if self.config.xi_func.model.cls == rlexp.approximation.ActionAndFeatureComponentSeparatedLinearNN:
            new_xi_func.model = eu.misc.create_object_from_config(
                self.config.xi_func.model,
                self.observation_space.shape[0],
                self.action_space.n,
                n_feature_components=self.n_feature_dim,
                n_out_per_component=self.config.xi_func.n_bins_per_feature
            )
        elif self.config.xi_func.model.cls == rlexp.approximation.ActionAndFeatureComponentSeparatedGammaNormalizedLinearNN:
            new_xi_func.model = eu.misc.create_object_from_config(
                self.config.xi_func.model,
                self.observation_space.shape[0],
                self.action_space.n,
                n_feature_components=self.n_feature_dim,
                n_out_per_component=self.config.xi_func.n_bins_per_feature,
                gamma=self.config.gamma
            )
        else:
            new_xi_func.model = eu.misc.create_object_from_config(
                self.config.xi_func.model,
                self.observation_space.shape[0],
                self.action_space.n,
                n_out = self.config.xi_func.n_bins_per_feature * self.n_feature_dim
            )

        # copy the parameters of the previous Q function
        if self.xi_func_per_reward_func and self.config.xi_func.init_mode == 'previous':
            new_xi_func.model.load_state_dict(self.xi_func_per_reward_func[self.active_reward_func_idx].model.state_dict())

        new_xi_func.optimizer = eu.misc.create_object_from_config(
            self.config.xi_func.optimizer,
            new_xi_func.model.parameters(),
        )

        self.xi_func_per_reward_func.append(new_xi_func)

        self.reward_per_feature_per_reward_func.append([])
        self._calc_reward_per_feature(new_rfunc_idx)

        return new_rfunc_idx


    def get_action(self, obs, info, exp_info):
        """Draws an epsilon-greedy policy"""

        obs = obs['observation']

        # line 10 - 14
        if self.config.use_gpi:
            action, self.c = self.calc_max_action(obs)
        else:
            action, self.c = self.calc_max_action(obs, policy_idx=self.active_reward_func_idx, target_reward_func_idx=self.active_reward_func_idx)

        if np.random.rand() < self.epsilon:
            action = np.random.choice(np.arange(self.action_space.n))

        return action


    def update(self, transition, exp_info):
        """Use the given transition to update the agent."""

        # super is updating the reward function if it is approximated
        super().update(transition, exp_info)

        active_xi_func = self.xi_func_per_reward_func[self.active_reward_func_idx]

        # extract transition information
        obs, action, reward, next_obs, done, _ = transition
        # identify the index of the feature combination
        feature = next_obs['feature']
        #feature_idxs = self.get_feature_idxs(feature)
        feature_encoding = self.get_feature_encoding(feature)

        # obs = obs['observation']
        # obs_full_mat = np.tile(obs, (self.config.xi_func.n_bins_per_feature*self.n_feature_dim, 1)).transpose()
        # obs_active_feature_mat = np.tile(obs, (self.n_feature_dim, 1)).transpose()
        # next_obs = next_obs['observation']
        obs_batch = torch.tensor([obs['observation']], dtype=active_xi_func.model.dtype)
        next_obs_batch = torch.tensor([next_obs['observation']], dtype=active_xi_func.model.dtype)
        next_obs = next_obs['observation']
        action = torch.tensor(action, dtype=torch.long)

        # if done:
        #     gamma = 0
        #     next_action = 0  # just a dummy action
        # else:
        #     gamma = self.config.gamma
        #     next_action, _ = self.calc_max_action(next_obs, target_reward_func_idx=self.active_reward_func_idx)
        if done:
            gamma = torch.tensor(0, dtype=active_xi_func.model.dtype)
            next_action = torch.tensor(0, dtype=torch.long)  # just a dummy action
        else:
            gamma = torch.tensor(self.config.gamma, dtype=active_xi_func.model.dtype)

            if self.config.use_gpi:
                next_action, _ = self.calc_max_action(next_obs, target_reward_func_idx=self.active_reward_func_idx)
            else:
                next_action, self.c = self.calc_max_action(next_obs, policy_idx=self.active_reward_func_idx, target_reward_func_idx=self.active_reward_func_idx)
            next_action = torch.tensor(next_action, dtype=torch.long)

        current_xi = active_xi_func.model(obs_batch)[0][action, :]
        with torch.no_grad():
            next_xi = active_xi_func.model(next_obs_batch)[0][next_action, :]

        expected_state_action_xi = feature_encoding + gamma * next_xi

        loss = self.xi_func_loss_func(current_xi, expected_state_action_xi)
        active_xi_func.optimizer.zero_grad()
        loss.backward()
        # avoid exploding weights and gradients using gradient clipping
        if self.config.xi_func.gradient_clipping_value is not None:
            torch.nn.utils.clip_grad_value_(active_xi_func.model.parameters(), clip_value=self.config.xi_func.gradient_clipping_value)
        # optimize the model
        active_xi_func.optimizer.step()

        # log the loss on the original function
        log.add_value('xi_func_loss_per_step', loss.item())

        # update previous policy, if it provided the gpi action
        if self.c != self.active_reward_func_idx and self.config.use_gpi:
            next_action, _ = self.calc_max_action(next_obs, policy_idx=self.c, target_reward_func_idx=self.c)
            next_action = torch.tensor(next_action, dtype=torch.long)

            gpi_optimal_xi_func = self.xi_func_per_reward_func[self.c]
            current_xi = gpi_optimal_xi_func.model(obs_batch)[0][action, :]
            with torch.no_grad():
                next_xi = gpi_optimal_xi_func.model(next_obs_batch)[0][next_action, :]
            expected_state_action_xi = feature_encoding + gamma * next_xi
            loss = self.xi_func_loss_func(current_xi, expected_state_action_xi)
            gpi_optimal_xi_func.optimizer.zero_grad()
            loss.backward()
            # avoid exploding weights and gradients using gradient clipping
            if self.config.xi_func.gradient_clipping_value is not None:
                torch.nn.utils.clip_grad_value_(gpi_optimal_xi_func.model.parameters(), clip_value=self.config.xi_func.gradient_clipping_value)
            # optimize the model
            gpi_optimal_xi_func.optimizer.step()

        if self.config.recalc_rewards_every_nth_step is not None and exp_info.step % self.config.recalc_rewards_every_nth_step == 0:
            self._calc_reward_per_feature()


    def _calc_reward_per_feature(self, reward_function_idx=None):
        """
        Calculates for each feature component the reward for each possible feature for the given or currently active reward function.
        """

        if reward_function_idx is None:
            reward_function_idx = self.active_reward_func_idx

        rewards = torch.empty(self.n_feature_dim * self.config.xi_func.n_bins_per_feature, dtype=self.xi_func_per_reward_func[reward_function_idx].model.dtype)
        rewards_idx = 0

        # go over feature dimensions
        for feature_dim in range(self.n_feature_dim):
            # calc the reward per dimension
            for median in self.medians_per_feature_dim[feature_dim]:
                rewards[rewards_idx] = self.get_reward_of_component(reward_function_idx, feature_dim, median)
                rewards_idx += 1

        self.reward_per_feature_per_reward_func[reward_function_idx] = rewards


    def calc_max_action(self, obs, policy_idx=None, target_reward_func_idx=None):
        """
        Get the optimal action for a given reward function.
        If policy_idx is None, then it gets the GPI optimal action
        """

        if policy_idx is None:
            policy_idx = list(range(len(self.xi_func_per_reward_func)))
        elif not isinstance(policy_idx, list):
            policy_idx = [policy_idx]

        if target_reward_func_idx is None:
            target_reward_func_idx = self.active_reward_func_idx

        # calculate the Q-values for all actions
        state_batch = torch.tensor([obs], dtype=self.xi_func_per_reward_func[0].model.dtype)

        q_values = np.zeros((len(policy_idx), self.action_space.n))
        for idx in range(len(policy_idx)):
            # xi = np.matmul(obs, self.z_per_reward_func[policy_idx[idx]])
            #
            # # xi-values are not allowed to be negative sums
            # xi = np.maximum(0.0, xi)
            #
            # # calculate the reward for each possible feature_combination
            # #reward_per_feature_comb = self.calc_reward_per_feature_comb(target_reward_func_idx=target_reward_func_idx)
            # q_values[idx, :] = np.matmul(xi, self.reward_per_feature_per_reward_func[target_reward_func_idx])
            with torch.no_grad():
                xi = self.xi_func_per_reward_func[policy_idx[idx]].model(state_batch)[0]
                xi = torch.max(torch.tensor(0.0), xi)
                q_values[idx, :] = np.matmul(xi, self.reward_per_feature_per_reward_func[target_reward_func_idx])

        # identify the optimal policy and action
        # select action with highest return over all policies
        max_value = np.max(q_values)
        where_max_value = np.where(q_values == max_value)
        n_max_values = len(where_max_value[0])
        if n_max_values == 1:
            selected_val_idx = 0
        else:
            selected_val_idx = np.random.randint(n_max_values)
        max_policy_idx = where_max_value[0][selected_val_idx]
        max_action = where_max_value[1][selected_val_idx]

        return max_action, policy_idx[max_policy_idx]


    def calc_xi_function(self, state, action, reward_function_idx=None):

        # if reward_function_idx is None:
        #     reward_function_idx = self.active_reward_func_idx
        #
        # xi = np.matmul(state, self.z_per_reward_func[reward_function_idx][action, :, :])
        # return xi

        if reward_function_idx is None:
            reward_function_idx = self.active_reward_func_idx

        state_batch = torch.tensor([state], dtype=self.xi_func_per_reward_func[reward_function_idx].model.dtype)

        xi = self.xi_func_per_reward_func[reward_function_idx].model(state_batch)[0]

        return xi[0].detach().numpy()


    def calc_expected_return(self, state, action=None, policy_idx='all', reward_func_idx=None):

        if reward_func_idx is None:
            reward_func_idx = self.active_reward_func_idx

        # if np.ndim(state) == 1:
        #     states = [state]
        # else:
        #     states = state
        if np.ndim(state) == 1:
            states = torch.tensor([state], dtype=self.xi_func_per_reward_func[0].model.dtype)
        else:
            states = torch.tensor(state, dtype=self.xi_func_per_reward_func[0].model.dtype)

        if action is not None:
            check_actions = [action]
        else:
            check_actions = [a for a in range(self.action_space.n)]

        if policy_idx == 'all':
            policy_idxs = [i for i in range(len(self.xi_func_per_reward_func))]
        else:
            policy_idxs = [policy_idx]

        q_values = np.full((np.shape(states)[0], len(policy_idxs), len(check_actions)), np.nan)
        for q_values_p_idx, p_idx in enumerate(policy_idxs):
            for a_idx, a in enumerate(check_actions):
                # xi = np.matmul(state, self.z_per_reward_func[p_idx][a, :, :])
                # # xi-values are not allowed to be negative sums
                # xi = np.maximum(0.0, xi)
                # # calculate the reward for each possible feature_combination
                # q_values[:, q_values_p_idx, a_idx] = np.matmul(xi, self.reward_per_feature_per_reward_func[reward_func_idx])
                with torch.no_grad():
                    xi = self.xi_func_per_reward_func[policy_idxs[q_values_p_idx]].model(states)[:, a, :]
                    xi = torch.max(torch.tensor(0.0), xi)
                    q_values[:, q_values_p_idx, a_idx] = torch.matmul(xi, self.reward_per_feature_per_reward_func[reward_func_idx]).detach().numpy()

        q_max = np.max(q_values, axis=1)
        if np.ndim(state) == 1:
            ret_val = q_max[0]
        else:
            ret_val = q_max

        if action is not None:
            ret_val = ret_val[0]

        return ret_val
