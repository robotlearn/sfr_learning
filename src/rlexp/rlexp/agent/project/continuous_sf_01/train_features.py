##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
import exputils as eu
import torch
import numpy as np
import exputils.data.logging as log
import os
import time


class TransitionDataSet(torch.utils.data.Dataset):

    @staticmethod
    def default_config():
        return eu.AttrDict(
            experiment_directory='./experiments',
            total_transistions_retention_rate=None, # how many transitions should be kept overall
            zero_reward_transitions_retention_rate=None, # how many non zero reward transitions should be kept
            observation_type='state_concatination', # 'state_concatination', 'state_diff', 'object_memory_diff', 'feature'
        )


    def __init__(self, experiment_id, repetition_idx, config=None, **kwargs):
        self.config = eu.combine_dicts(kwargs, config, self.default_config())

        experiment_directory_template = os.path.join(
            self.config.experiment_directory,
            eu.EXPERIMENT_DIRECTORY_TEMPLATE,
            eu.REPETITION_DIRECTORY_TEMPLATE
        )

        # load the dataset
        data = eu.data.loading.load_single_experiment_data(
            experiment_directory=experiment_directory_template.format(experiment_id, repetition_idx),
            allowed_data_filter=['transitions', 'phase_per_transition'],
            allow_pickle=True
        )
        transitions = data.transitions
        phase_per_transition = data.phase_per_transition

        # filter out transitions that have a reward of 0 to have 75% of the repetitions to have a positive reward
        rewards = transitions[:, 2]

        if self.config.zero_reward_transitions_retention_rate is None or self.config.zero_reward_transitions_retention_rate == 1.0:
            transition_idxs = np.arange(len(rewards))
        else:
            zero_reward_transition_idxs = np.where(rewards == 0.0)[0]
            nonzero_reward_transition_idxs = np.where(rewards != 0.0)[0]

            n_filtered_zero_reward_transitions = int(len(zero_reward_transition_idxs) * self.config.zero_reward_transitions_retention_rate)
            filtered_zero_reward_transitions = np.random.choice(zero_reward_transition_idxs, n_filtered_zero_reward_transitions, replace=False)
            transition_idxs = np.concatenate((filtered_zero_reward_transitions, nonzero_reward_transition_idxs))

        np.random.shuffle(transition_idxs)  # random order of transitions

        if self.config.total_transistions_retention_rate is not None and self.config.total_transistions_retention_rate < 1.0:
            n_kept_transitions = int(self.config.total_transistions_retention_rate * len(transition_idxs))
            transition_idxs = transition_idxs[:n_kept_transitions]

        # collect the observations (state + next_state) and rewards from the transitions
        n_obs_dim = len(transitions[0, 0]['observation'])
        n_tasks = np.max(phase_per_transition) + 1

        if self.config.observation_type == 'state_concatination':
            obs_dim = n_obs_dim * 2
            obs_func = lambda state, next_state: torch.cat([torch.tensor(state['observation']), torch.tensor(next_state['observation'])])
        elif self.config.observation_type == 'state_diff':
            obs_dim = n_obs_dim
            obs_func = lambda state, next_state: torch.tensor(next_state['observation']) - torch.tensor(state['observation'])
        elif self.config.observation_type == 'object_memory_diff':
            if n_obs_dim == 113:
                obs_dim = 12
                obs_func = lambda state, next_state: torch.tensor(next_state['observation'])[100:112] - torch.tensor(state['observation'])[100:112]
            else:
                ValueError('Wrong dimension of observations for config.observation_type \'object_memory_diff\'!')
        elif self.config.observation_type == 'pos_and_object_memory_diff':
            if n_obs_dim == 113:
                obs_dim = 100 + 12
                obs_func = lambda state, next_state: torch.cat([
                    torch.tensor(next_state['observation'])[0:100],
                    torch.tensor(next_state['observation'])[100:112] - torch.tensor(state['observation'])[100:112]
                ])
            else:
                ValueError('Wrong dimension of observations for config.observation_type \'pos_and_object_memory_diff\'!')
        elif self.config.observation_type == 'feature':
            obs_dim = len(transitions[0, 0]['feature'])
            obs_func = lambda state, next_state: torch.tensor(next_state['feature'])
        else:
            raise ValueError('Unknown config.observation_type!')

        self.observations = torch.empty((len(transition_idxs), obs_dim), dtype=torch.float32)
        self.rewards = torch.zeros((len(transition_idxs), n_tasks), dtype=torch.float32)
        self.reward_masks = torch.zeros((len(transition_idxs), n_tasks), dtype = torch.bool)
        for sample_idx, transition_idx in enumerate(transition_idxs):

            self.observations[sample_idx, :] = obs_func(transitions[transition_idx, 0], transitions[transition_idx, 3])
            self.rewards[sample_idx, phase_per_transition[transition_idx]] = rewards[transition_idx]
            self.reward_masks[sample_idx, phase_per_transition[transition_idx]] = True


    def __len__(self):
        return len(self.rewards)


    def __getitem__(self, idx):
        return self.observations[idx], self.rewards[idx], self.reward_masks[idx]


    @staticmethod
    def adapt_reward_output(model_output, expected_reward_output, reward_mask):
        """
        Set the rewards for all other tasks, besides the one used in the transition, to the ones computed by the model.
        Thus, only the loss of the reward and weight vector for the task used in the transition is computed.
        """
        adapted_output = model_output.detach().clone()
        adapted_output[reward_mask] = expected_reward_output[reward_mask]
        return adapted_output


# class FeatureModel(torch.nn.Module):
#
#     @staticmethod
#     def default_config():
#         return eu.AttrDict(
#             init_sigma = 0.1,
#         )
#
#     def __init__(self, n_in, n_tasks, n_feature_dim, observation_type='not defined', config=None, **kwargs):
#         super(FeatureModel, self).__init__()
#         self.config = eu.combine_dicts(kwargs, config, self.default_config())
#
#         self.register_parameter(
#             name='H',
#             param=torch.nn.Parameter(torch.randn((n_in, n_feature_dim), dtype=torch.float32) * self.config.init_sigma)
#         )
#
#         self.register_parameter(
#             name='W',
#             param=torch.nn.Parameter(torch.randn((n_feature_dim, n_tasks), dtype=torch.float32) * self.config.init_sigma)
#         )
#
#         self.observation_type = observation_type
#
#
#     def forward(self, input):
#         feature = torch.sigmoid_(torch.matmul(input, self.H))
#         reward = torch.matmul(feature, self.W)
#         return reward
#
#
#     def calc_feature(self, input):
#         feature = torch.sigmoid_(torch.matmul(input, self.H))
#         return feature

class LinearFeatureModel(torch.nn.Module):

    @staticmethod
    def default_config():
        return eu.AttrDict(
            init_sigma = 0.1,
        )

    def __init__(self, n_in, n_feature_dim, observation_type='not defined', config=None, **kwargs):
        super(LinearFeatureModel, self).__init__()
        self.config = eu.combine_dicts(kwargs, config, self.default_config())

        self.register_parameter(
            name='H',
            param=torch.nn.Parameter(torch.randn((n_in, n_feature_dim), dtype=torch.float32) * self.config.init_sigma)
        )

        self.observation_type = observation_type


    def forward(self, input):
        feature = torch.sigmoid_(torch.matmul(input, self.H))
        return feature



class DeepFeatureModel(torch.nn.Module):

    @staticmethod
    def default_config():
        return eu.AttrDict(
            hidden_layers=None,
            activation_func=torch.nn.ReLU,
            is_bias=True,
            weight_init_func=None,
        )

    def __init__(self, n_in, n_feature_dim, observation_type='not defined', config=None, **kwargs):
        super(DeepFeatureModel, self).__init__()
        self.config = eu.combine_dicts(kwargs, config, self.default_config())
        self.n_obs_dim = n_in
        self.n_feature_dim = n_feature_dim


        self.observation_type = observation_type

        self.layers = torch.nn.ModuleList()

        # no hidden layer of nothing is defined
        if self.config.hidden_layers is None or len(self.config.hidden_layers) == 0:
            self.layers.append(self._create_linear_layer(self.n_obs_dim, n_feature_dim))
        else:
            next_n_in = self.n_obs_dim
            for n_hidden_neurons in self.config.hidden_layers:
                self.layers.append(self._create_linear_layer(next_n_in, n_hidden_neurons))
                if self.config.activation_func is not None:
                    self.layers.append(self.config.activation_func())
                next_n_in = n_hidden_neurons
            # output
            self.layers.append(self._create_linear_layer(next_n_in, n_feature_dim))

    def _create_linear_layer(self, n_in, n_out):
        linear_layer = torch.nn.Linear(n_in, n_out, bias=True)
        if self.config.weight_init_func is not None:
            eu.misc.call_function_from_config(self.config.weight_init_func, linear_layer.weight)
        return linear_layer

    def forward(self, input):

        if isinstance(input, np.ndarray):
            is_numpy = True
        else:
            is_numpy = False

        if is_numpy:
            input = torch.tensor(input, dtype=torch.float32)

        # outputs are for [state_idx, action_idx, feature_idx]
        output = input
        for layer in self.layers:
            output = layer(output)

        feature = torch.sigmoid_(output)

        if is_numpy:
            feature = feature.detach().cpu().numpy()

        return feature



def train_features(experiment_id, repetition_idx, config=None, **kwargs):

    default_config = eu.AttrDict(
        seed=637264278,

        n_feature_dim=4,

        n_iter=300000,

        dataset=eu.AttrDict(
            cls=TransitionDataSet,
        ),

        dataloader=eu.AttrDict(
            cls=torch.utils.data.DataLoader,
            batch_size=16,
            shuffle=True
        ),

        w_init_sigma = 0.05,

        model=eu.AttrDict(
            cls=DeepFeatureModel,
        ),

        optimizer=eu.AttrDict(
            cls=torch.optim.SGD,
            lr=30,
        ),

        loss_function=eu.AttrDict(
            cls=torch.nn.MSELoss,
        ),

        reward_weight_normalization = None, # None, L1, L2

        log_directory = None,

    )
    config = eu.combine_dicts(kwargs, config, default_config)

    eu.misc.seed(config)

    # dataset
    dataset = eu.misc.create_object_from_config(
        config.dataset,
        experiment_id,
        repetition_idx
    )

    dataloader = eu.misc.create_object_from_config(
        config.dataloader,
        dataset,
    )

    # create model, optimizer, and loss
    n_obs_dim = len(dataset[0][0])
    n_tasks = len(dataset[0][1])

    model = eu.misc.create_object_from_config(
        config.model,
        n_in=n_obs_dim, # length of observation
        n_feature_dim=config.n_feature_dim,
        observation_type=dataset.config.observation_type
    )

    W = torch.nn.Parameter(torch.randn((config.n_feature_dim, n_tasks), dtype=torch.float32) * config.w_init_sigma)

    optimizer = eu.misc.create_object_from_config(
        config.optimizer,
        list(model.parameters()) + [W]  # optimize feature model params and W
    )

    loss_function = eu.misc.create_object_from_config(config.loss_function)

    start_time_in_sec = time.time()

    # iterate over dataset
    for i in range(config.n_iter):

        # get batch of data
        model_input, model_expected_output, reward_task_mask = next(iter(dataloader))

        # calculate expected rewards by model
        modeled_features = model(model_input)

        # calculate the reward using the weight W
        modeled_rewards = torch.matmul(modeled_features, W)

        # Set the rewards for all other tasks, besides the one used in the transition, to the ones computed by the model.
        # Thus, only the loss of the reward and weight vector for the task used in the transition is computed.
        adapted_model_expected_output = dataset.adapt_reward_output(modeled_rewards, model_expected_output, reward_task_mask)

        loss = loss_function(modeled_rewards, adapted_model_expected_output)

        log.add_value('training_loss', loss.item())

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if config.reward_weight_normalization and config.reward_weight_normalization != 'None' and config.reward_weight_normalization != '':

            with torch.no_grad():
                if config.reward_weight_normalization == 'L2':
                    w_norm = torch.norm(W, dim=0, keepdim=True)
                else:
                    raise ValueError('Unknown Reward Weight Initialization!')

                w_norm[w_norm < 1e-6] = 1e-6

                W = torch.nn.Parameter(W / w_norm)

    end_time_in_sec = time.time()
    elapsed_time = end_time_in_sec - start_time_in_sec
    log.add_value('time', elapsed_time)

    log.add_single_object('final_model', model, directory=config.log_directory)
    log.add_single_object('final_weights', W, directory=config.log_directory)

    log.save(directory=config.log_directory)

    return model
