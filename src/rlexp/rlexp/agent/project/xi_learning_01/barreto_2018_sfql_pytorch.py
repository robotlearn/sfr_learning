##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
import numpy as np
import rlexp
import exputils as eu
import gym
import torch

class Barreto2018SFQLPyTorch:
    """
    Successor Feature Q-learning agent for continual reward transfer scenarios that uses a single linear layer to
    approximate the Psi-function. Based on SF QL agent in paper [Barreto 2018].
    (https://arxiv.org/pdf/1606.05312.pdf)

    Uses PyTorch for the implementation of the function approximators.
    """

    @staticmethod
    def default_config():
        dc = eu.AttrDict(
            epsilon=0.15,  # can also be a dict that defines the function to get epsilon
            alpha_w=0.05,
            gamma=0.95,
            w_init_std=0.01,
            is_set_initial_reward_weights=False,

            psi_func=eu.AttrDict(
                model=eu.AttrDict(
                    cls=rlexp.approximation.ActionSeparatedLinearNN,
                ),

                optimizer=eu.AttrDict(
                    cls=torch.optim.SGD,
                    lr=0.01,
                ),

                loss=eu.AttrDict(
                    cls=torch.nn.MSELoss,
                ),

                gradient_clipping_value=1.0,
            ),

        )
        return dc
        

    def __init__(self, env, config=None, **kwargs):
        self.config = eu.combine_dicts(kwargs, config, self.default_config())

        self.alpha_w = self.config.alpha_w
        self.epsilon = self.config.epsilon

        self.w_per_reward_func = []

        self.active_reward_func_idx = None

        self.c = None

        ########
        # pytorch code

        self.psi_func_per_reward_func = []

        # loss function for the psi functions (default is MSE loss)
        self.psi_func_loss_func = eu.misc.call_function_from_config(
            self.config.psi_func.loss,
            func_attribute_name='cls'
        )
        ########

        # obs, feature and action space
        if not isinstance(env.observation_space, gym.spaces.Dict) and 'observation' in env.observation_space.spaces and 'feature' in env.observation_space.spaces:
            raise ValueError('Only Dict observation spaces are support that have a \'observation\' and \'feature\' attribute!')

        self.observation_space = env.observation_space.spaces['observation']
        self.feature_space = env.observation_space.spaces['feature']
        self.action_space = env.action_space

        if isinstance(self.feature_space, gym.spaces.MultiDiscrete):
            self.feature_len = len(self.feature_space.nvec)
        elif isinstance(self.feature_space, gym.spaces.MultiBinary):
            self.feature_len = self.feature_space.n
        elif isinstance(self.feature_space, gym.spaces.Box):
            self.feature_len = self.feature_space.shape[0]
        else:
            raise ValueError('Nonsupported feature_space object!')


    def add_reward_function(self, reward_function, reward_func_descr=None):

        if self.config.is_set_initial_reward_weights:
            # use given reward weight vector
            w = torch.tensor(reward_func_descr['reward_weight_vector'].copy(), dtype=torch.float32)
        else:
            # set up initial weight vector for the new reward function
            # line 2
            w = torch.tensor(np.random.randn(self.feature_len) * self.config.w_init_std, dtype=torch.float32)
        self.w_per_reward_func.append(w)

        # set up initial psi-function for the new reward function
        # line 3
        new_psi_func = eu.AttrDict()

        new_psi_func.model = eu.misc.call_function_from_config(
            self.config.psi_func.model,
            self.observation_space.shape[0],
            self.action_space.n,
            self.feature_len,
            func_attribute_name='cls'
        )

        # copy the parameters of the previous function
        if self.psi_func_per_reward_func:
            new_psi_func.model.load_state_dict(self.psi_func_per_reward_func[self.active_reward_func_idx].model.state_dict())

        new_psi_func.optimizer = eu.misc.call_function_from_config(
            self.config.psi_func.optimizer,
            new_psi_func.model.parameters(),
            func_attribute_name='cls'
        )

        self.psi_func_per_reward_func.append(new_psi_func)

        return len(self.psi_func_per_reward_func) - 1


    def set_active_reward_func_idx(self, idx):
        self.active_reward_func_idx = idx


    def get_action(self, obs, info, exp_info):
        """Draws an epsilon-greedy policy"""

        obs = obs['observation']

        # line 10 - 14
        action, self.c = self.calc_max_action(obs)
        if np.random.rand() < self.epsilon:
            action = np.random.choice(np.arange(self.action_space.n))

        return action


    def update(self, transition, exp_info):
        """Use the given transition to update the agent."""

        active_psi_func = self.psi_func_per_reward_func[self.active_reward_func_idx]

        # extract transition information
        obs, action, reward, next_obs, done, _ = transition
        feature = torch.tensor(next_obs['feature'], dtype=active_psi_func.model.dtype)
        obs_batch = torch.tensor([obs['observation']], dtype=active_psi_func.model.dtype)
        next_obs_batch = torch.tensor([next_obs['observation']], dtype=active_psi_func.model.dtype)
        next_obs = next_obs['observation']
        action = torch.tensor(action, dtype=torch.long)

        # get policy c from which the current gpi action was taken
        # line 10
        c = self.c

        # lines 15 - 20
        if done:
            gamma = torch.tensor(0, dtype=active_psi_func.model.dtype)
            next_action = torch.tensor(0, dtype=torch.long)  # just a dummy action
        else:
            gamma = torch.tensor(self.config.gamma, dtype=active_psi_func.model.dtype)
            next_action, _ = self.calc_max_action(next_obs, target_reward_func_idx=self.active_reward_func_idx)
            next_action = torch.tensor(next_action, dtype=torch.long)

        # update the reward weights
        # line 21
        w = self.w_per_reward_func[self.active_reward_func_idx]
        w = w + self.alpha_w * (reward - torch.matmul(feature, w)) * feature
        self.w_per_reward_func[self.active_reward_func_idx] = w

        # update current psi function
        # lines 22 - 23
        current_psi = active_psi_func.model(obs_batch)[0][action, :]
        with torch.no_grad():
            next_psi = active_psi_func.model(next_obs_batch)[0][next_action, :]
        expected_state_action_psi = feature + (gamma * next_psi)
        loss = self.psi_func_loss_func(current_psi, expected_state_action_psi)
        active_psi_func.optimizer.zero_grad()
        loss.backward()
        # avoid exploding weights and gradients using gradient clipping
        if self.config.psi_func.gradient_clipping_value is not None:
            torch.nn.utils.clip_grad_value_(active_psi_func.model.parameters(), clip_value=self.config.psi_func.gradient_clipping_value)
        # optimize the model
        active_psi_func.optimizer.step()

        # update previous policy, if it provided the gpi action
        # lines 25 - 30
        if c != self.active_reward_func_idx:
            # line 26
            next_action, _ = self.calc_max_action(next_obs, policy_idx=c, target_reward_func_idx=c)
            next_action = torch.tensor(next_action, dtype=torch.long)

            # lines 27 - 28
            gpi_optimal_psi_func = self.psi_func_per_reward_func[c]
            current_psi = gpi_optimal_psi_func.model(obs_batch)[0][action, :]
            with torch.no_grad():
                next_psi = gpi_optimal_psi_func.model(next_obs_batch)[0][next_action, :]
            expected_state_action_psi = feature + (gamma * next_psi)
            loss = self.psi_func_loss_func(current_psi, expected_state_action_psi)
            gpi_optimal_psi_func.optimizer.zero_grad()
            loss.backward()
            # avoid exploding weights and gradients using gradient clipping
            if self.config.psi_func.gradient_clipping_value is not None:
                torch.nn.utils.clip_grad_value_(gpi_optimal_psi_func.model.parameters(), clip_value=self.config.psi_func.gradient_clipping_value)
            # optimize the model
            gpi_optimal_psi_func.optimizer.step()


    def calc_max_action(self, state, policy_idx=None, target_reward_func_idx=None):
        """
        Get the optimal action for a given reward function.
        If policy_idx is None, then it gets the GPI optimal action
        """

        if policy_idx is None:
            policy_idx = list(range(len(self.psi_func_per_reward_func)))
        elif not isinstance(policy_idx, list):
            policy_idx = [policy_idx]

        if target_reward_func_idx is None:
            target_reward_func_idx = self.active_reward_func_idx

        # calculate the Q-values for all actions
        # Q = phi * z * w
        state_batch = torch.tensor([state], dtype=self.psi_func_per_reward_func[0].model.dtype)

        q_values = np.zeros((len(policy_idx), self.action_space.n))
        for idx in range(len(policy_idx)):
            with torch.no_grad():
                psi = self.psi_func_per_reward_func[policy_idx[idx]].model(state_batch)[0]
                q_values[idx, :] = torch.matmul(psi, self.w_per_reward_func[target_reward_func_idx]).detach().numpy()

        # identify the optimal policy and action
        # select action with highest return over all policies
        max_value = np.max(q_values)
        where_max_value = np.where(q_values == max_value)
        n_max_values = len(where_max_value[0])
        if n_max_values == 1:
            selected_val_idx = 0
        else:
            selected_val_idx = np.random.randint(n_max_values)
        max_policy_idx = where_max_value[0][selected_val_idx]
        max_action = where_max_value[1][selected_val_idx]

        return max_action, policy_idx[max_policy_idx]


    # def calc_psi_function(self, state, action, reward_function_idx=None):
    #
    #     if reward_function_idx is None:
    #         reward_function_idx = self.active_reward_func_idx
    #
    #     psi = np.matmul(state, self.z_per_reward_func[reward_function_idx][action, :, :])
    #     return psi


    def calc_expected_return(self, state, action=None, policy_idx='all', reward_func_idx=None):

        if reward_func_idx is None:
            reward_func_idx = self.active_reward_func_idx

        if np.ndim(state) == 1:
            states = torch.tensor([state], dtype=self.psi_func_per_reward_func[0].model.dtype)
        else:
            states = torch.tensor(state, dtype=self.psi_func_per_reward_func[0].model.dtype)

        if action is not None:
            check_actions = [action]
        else:
            check_actions = [a for a in range(self.action_space.n)]

        if policy_idx == 'all':
            policy_idxs = [i for i in range(len(self.psi_func_per_reward_func))]
        else:
            policy_idxs = [policy_idx]

        q_values = np.full((np.shape(states)[0], len(policy_idxs), len(check_actions)), np.nan)
        for q_values_p_idx, p_idx in enumerate(policy_idxs):
            for a_idx, a in enumerate(check_actions):
                with torch.no_grad():
                    psi = self.psi_func_per_reward_func[policy_idxs[q_values_p_idx]].model(states)[:, a, :]
                    q_values[:, q_values_p_idx, a_idx] = torch.matmul(psi, self.w_per_reward_func[reward_func_idx]).detach().numpy()

        q_max = np.max(q_values, axis=1)
        if np.ndim(state) == 1:
            ret_val = q_max[0]
        else:
            ret_val = q_max

        if action is not None:
            ret_val = ret_val[0]

        return ret_val