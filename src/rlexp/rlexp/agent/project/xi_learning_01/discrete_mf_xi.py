##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
import numpy as np
import exputils as eu
import warnings
from rlexp.agent.project.xi_learning_01 import DiscreteFeatureRewardFuncApproxBase


class DiscreteMFXi(DiscreteFeatureRewardFuncApproxBase):
    """
    Model-free Xi agent that uses a linear mapping to represent the Xi-function.
    """

    @staticmethod
    def default_config():
        dc = eu.AttrDict(
            alpha = 0.05,
            epsilon = 0.15, # can also be a dict that defines the function to get epsilon
            gamma = 0.95,
            z_init_std = 0.01,
            z_init = None,
            absolute_weight_maximum = 1000,  # maximum absolute value of weights used for w and z
            is_set_initial_reward_weights=False,
        )

        # get default config from base class
        dc = eu.combine_dicts(
            dc,
            DiscreteFeatureRewardFuncApproxBase.default_config())

        return dc
        

    def __init__(self, env, config=None, **kwargs):
        config = eu.combine_dicts(kwargs, config, self.default_config())
        super().__init__(env, config=config)

        self.alpha = self.config.alpha
        self.epsilon = self.config.epsilon
        self.z_per_reward_func = []
        self.c = None


    def add_reward_function(self, reward_function, reward_func_descr=None):

        new_rfunc_idx = super().add_reward_function(reward_function, reward_func_descr=reward_func_descr)

        # set up initial psi-function for the new reward function
        if not self.z_per_reward_func:
            if self.config.z_init is None:
                z = np.random.randn(self.action_space.n, self.observation_space.shape[0], self.n_feature_combinations) * self.config.z_init_std
            else:
                z = self.config.z_init.copy()
        else:
            z = self.z_per_reward_func[self.active_reward_func_idx].copy()
        self.z_per_reward_func.append(z)

        return new_rfunc_idx


    def get_action(self, obs, info, exp_info):
        """Draws an epsilon-greedy policy"""

        obs = obs['observation']

        # line 10 - 14
        action, self.c = self.calc_max_action(obs)
        if np.random.rand() < self.epsilon:
            action = np.random.choice(np.arange(self.action_space.n))

        return action


    def update(self, transition, exp_info):
        """Use the given transition to update the agent."""

        # super is updating the reward function if it is approximated
        super().update(transition, exp_info)

        # extract transition information
        obs, action, reward, next_obs, done, _ = transition
        # identify the index of the feature combination
        feature = next_obs['feature']
        feature_combination_idx = np.ravel_multi_index(feature.astype(int), self.n_feature_values)
        obs = obs['observation']
        next_obs = next_obs['observation']

        if done:
            gamma = 0
            next_action = 0  # just a dummy action
        else:
            gamma = self.config.gamma
            next_action, _ = self.calc_max_action(next_obs, target_reward_func_idx=self.active_reward_func_idx)

        # update current psi function
        z = self.z_per_reward_func[self.active_reward_func_idx]
        current_xi = np.matmul(obs, z[action, :])
        next_xi = np.matmul(next_obs, z[next_action, :])
        for k in range(self.n_feature_combinations):
            if feature_combination_idx == k:
                z[action, :, k] = z[action, :, k] + self.alpha * (1.0 + gamma * next_xi[k] - current_xi[k]) * obs
            else:
                z[action, :, k] = z[action, :, k] + self.alpha * (0.0 + gamma * next_xi[k] - current_xi[k]) * obs
        self.enforce_weight_maximum(z)

        # update previous policy, if it provided the gpi action
        if self.c != self.active_reward_func_idx:
            # line 26
            next_action, _ = self.calc_max_action(next_obs, policy_idx=self.c, target_reward_func_idx=self.c)

            # lines 27 - 28
            z = self.z_per_reward_func[self.c]
            current_xi = np.matmul(obs, z[action, :])
            next_xi = np.matmul(next_obs, z[next_action, :])
            for k in range(self.n_feature_combinations):
                if feature_combination_idx == k:
                    z[action, :, k] = z[action, :, k] + self.alpha * (1.0 + gamma * next_xi[k] - current_xi[k]) * obs
                else:
                    z[action, :, k] = z[action, :, k] + self.alpha * (0.0 + gamma * next_xi[k] - current_xi[k]) * obs
            self.enforce_weight_maximum(z)


    def calc_max_action(self, obs, policy_idx=None, target_reward_func_idx=None):
        """
        Get the optimal action for a given reward function.
        If policy_idx is None, then it gets the GPI optimal action
        """

        if policy_idx is None:
            policy_idx = list(range(len(self.z_per_reward_func)))
        elif not isinstance(policy_idx, list):
            policy_idx = [policy_idx]

        if target_reward_func_idx is None:
            target_reward_func_idx = self.active_reward_func_idx

        # calculate the Q-values for all actions
        # Q = phi * z * w
        q_values = np.zeros((len(policy_idx), self.action_space.n))
        for idx in range(len(policy_idx)):
            xi = np.matmul(obs, self.z_per_reward_func[policy_idx[idx]])

            # xi-values are not allowed to be negative sums
            xi = np.maximum(0.0, xi)

            rewards_of_all_feature_combs = self.get_rewards_of_all_feature_combs(target_reward_func_idx)
            q_values[idx, :] = np.matmul(xi, rewards_of_all_feature_combs)

        # identify the optimal policy and action
        # select action with highest return over all policies
        max_value = np.max(q_values)
        where_max_value = np.where(q_values == max_value)
        n_max_values = len(where_max_value[0])
        if n_max_values == 1:
            selected_val_idx = 0
        else:
            selected_val_idx = np.random.randint(n_max_values)
        max_policy_idx = where_max_value[0][selected_val_idx]
        max_action = where_max_value[1][selected_val_idx]

        return max_action, policy_idx[max_policy_idx]


    def calc_xi_function(self, state, action, reward_function_idx=None):

        if reward_function_idx is None:
            reward_function_idx = self.active_reward_func_idx

        xi = np.matmul(state, self.z_per_reward_func[reward_function_idx][action, :, :])
        return xi


    def calc_expected_return(self, state, action=None, policy_idx='all', reward_func_idx=None):

        if reward_func_idx is None:
            reward_func_idx = self.active_reward_func_idx

        if np.ndim(state) == 1:
            states = [state]
        else:
            states = state

        if action is not None:
            check_actions = [action]
        else:
            check_actions = [a for a in range(self.action_space.n)]

        if policy_idx == 'all':
            policy_idxs = [i for i in range(len(self.z_per_reward_func))]
        else:
            policy_idxs = [policy_idx]

        q_values = np.full((np.shape(states)[0], len(policy_idxs), len(check_actions)), np.nan)
        for q_values_p_idx, p_idx in enumerate(policy_idxs):
            for a in check_actions:
                xi = np.matmul(state, self.z_per_reward_func[p_idx][a, :, :])
                # xi-values are not allowed to be negative sums
                xi = np.maximum(0.0, xi)
                rewards_of_all_feature_combs = self.get_rewards_of_all_feature_combs(reward_func_idx)
                q_values[:, q_values_p_idx, a] = np.matmul(xi, rewards_of_all_feature_combs)

        q_max = np.max(q_values, axis=1)
        if np.ndim(state) == 1:
            ret_val = q_max[0]
        else:
            ret_val = q_max

        return ret_val


    def enforce_weight_maximum(self, weights):

        if self.config.absolute_weight_maximum is not None:

            weight_above_maximum_inds = weights > self.config.absolute_weight_maximum
            if np.any(weight_above_maximum_inds):
                weights[weight_above_maximum_inds] = self.config.absolute_weight_maximum
                warnings.warn('Some weights have reached the maximum value of {}.'.format(self.config.absolute_weight_maximum))

            weight_above_maximum_inds = weights < -1 * self.config.absolute_weight_maximum
            if np.any(weight_above_maximum_inds):
                weights[weight_above_maximum_inds] = -1 * self.config.absolute_weight_maximum
                warnings.warn('Some weights have reached the maximum value of {}.'.format(-1 * self.config.absolute_weight_maximum))
