##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
import numpy as np
import rlexp
import rlexp.utils as utils
import exputils as eu
import exputils.data.logging as log
import torch
import gym
import random

class DNNQLearning:
    """
    Q-learning agent for continual reward transfer scenarios that uses deep neural networks to approximate the Q-function.

    Config parameters:
        q_func:
            init_mode: String to specify how to initialize a new q function. (default='random')
                - 'random': Use random initialization from Q-function model.
                - 'previous': Reuse the weights from the Q-function model that was used for the previous reward
                              function.
    """

    @staticmethod
    def default_config():
        dc = eu.AttrDict(
            epsilon=0.15,  # can also be a dict that defines the function to get epsilon
            epsilon_step_counter='total_episodes',  # total_episodes
            gamma=0.95,

            q_func = eu.AttrDict(
                model = eu.AttrDict(
                    cls=rlexp.approximation.ActionSeparatedLinearNN,
                ),

                optimizer = eu.AttrDict(
                    cls=torch.optim.SGD,
                    lr=0.01,
                ),

                loss = eu.AttrDict(
                    cls=torch.nn.MSELoss,
                ),

                replay_buffer = eu.AttrDict(
                    cls=rlexp.approximation.ReplayBuffer,
                ),

                n_iterations_per_training = 1,

                init_mode='random',     # 'random' or 'previous': random crea

                gradient_clipping_value=1.0,
            ),
        )
        return dc


    def __init__(self, env, config=None, **kwargs):
        self.config = eu.combine_dicts(kwargs, config, self.default_config())

        # handle epsilon from config
        if self.config.epsilon_step_counter is not None and self.config.epsilon_step_counter not in ['total_episodes',
                                                                                                     'episodes_per_reward_function',
                                                                                                     '']:
            raise ValueError(
                'Invalid value {!r} for epsilon_step_counter configuration! Accepted values: \'total_episodes\', \'episodes_per_reward_function\'.'.format(
                    self.config.epsilon_step_counter))

        if not self.config.epsilon_step_counter:
            self.epsilon = self.config.epsilon
        else:
            self.epsilon = 0.0

        if not self.config.q_func.init_mode in ['random', 'previous']:
            raise ValueError('Invalid Configuration Parameter: q_func.init_mode ({!r}) can only be \'random\', \'previous\'!'.format(self.config.q_func.init_mode))

        self.observation_space = env.observation_space
        if isinstance(self.observation_space, gym.spaces.Dict) and 'observation' in self.observation_space.spaces:
            self.is_observation_subdict = True
            self.n_obs_dim = self.observation_space.spaces['observation'].shape[0]
        else:
            self.is_observation_subdict = False
            self.n_obs_dim = self.observation_space.shape[0]

        self.action_space = env.action_space

        self.active_reward_func_idx = None
        self.q_func_per_reward_func = []

        self.episodes_per_reward_func_counter = []
        self.episodes_counter = 0

   
    def add_reward_function(self, reward_function, reward_func_descr=None):

        self.episodes_per_reward_func_counter.append(0)

        # set up initial q-function for the new reward function
        new_q_func = eu.AttrDict()

        new_q_func.model = eu.misc.call_function_from_config(
            self.config.q_func.model,
            self.n_obs_dim,
            self.action_space.n,
            func_attribute_name='cls'
        )

        # copy the parameters of the previous Q function
        if self.config.q_func.init_mode == 'previous' and self.q_func_per_reward_func:
            new_q_func.model.load_state_dict(self.q_func_per_reward_func[-1].model.state_dict())

        new_q_func.optimizer = eu.misc.call_function_from_config(
            self.config.q_func.optimizer,
            new_q_func.model.parameters(),
            func_attribute_name='cls'
        )

        new_q_func.loss_func = eu.misc.call_function_from_config(
            self.config.q_func.loss,
            func_attribute_name='cls'
        )

        new_q_func.replay_buffer = eu.misc.call_function_from_config(
            self.config.q_func.replay_buffer,
            func_attribute_name='cls'
        )

        self.q_func_per_reward_func.append(new_q_func)

        # return new rfunc index
        return len(self.q_func_per_reward_func) - 1


    def set_active_reward_func_idx(self, idx):
        self.active_reward_func_idx = idx


    def start_episode(self, obs, exp_info):

        # set epsilon according to current episode
        if self.config.epsilon_step_counter == 'total_episodes':
            self.epsilon = utils.set_value_by_function(
                self.config.epsilon,
                self.episodes_counter)

        elif self.config.epsilon_step_counter == 'episodes_per_reward_function':
            self.epsilon = utils.set_value_by_function(
                self.config.epsilon,
                self.episodes_per_reward_func_counter[self.active_reward_func_idx])

        log.add_value('agent_epsilon_per_episode', self.epsilon)

        # increase internal counters of episodes
        self.episodes_per_reward_func_counter[self.active_reward_func_idx] += 1
        self.episodes_counter += 1


    def get_action(self, obs, info, exp_info):
        """Epsilon-greedy policy"""

        if self.is_observation_subdict:
            obs = obs['observation']

        if np.random.rand() < self.epsilon:
            action = np.random.choice(np.arange(self.action_space.n))
        else:
            action = self.calc_max_action(obs)

        return action


    def update(self, transition, exp_info):
        """Use the given transition to update the agent"""

        obs, action, reward, next_obs, done, info = transition

        if self.is_observation_subdict:
            obs = obs['observation']
            next_obs = next_obs['observation']

        active_q_func = self.q_func_per_reward_func[self.active_reward_func_idx]

        # add transition to replay buffer for active reward function
        data = (torch.tensor(obs, dtype=active_q_func.model.dtype),
                torch.tensor([action], dtype=torch.long),
                torch.tensor(next_obs, dtype=active_q_func.model.dtype),
                torch.tensor(reward, dtype=torch.float32),
                torch.tensor(not done, dtype=torch.bool))
        active_q_func.replay_buffer.add(data)

        # train the model
        self.train()


    def calc_max_action(self, state, reward_func_idx=None):
        """Select the optimal action for the given state based on its Q-values."""

        if reward_func_idx is None:
            reward_func_idx = self.active_reward_func_idx

        q_values = self.calc_q(state, reward_func_idx=reward_func_idx)
        _, action = rlexp.utils.select_max_value(q_values)
        return action


    def train(self, n_iterations=None):
        """
        Trains the approximator for n_iterations (either specified as attribute or in config) from samples
        """

        active_q_func = self.q_func_per_reward_func[self.active_reward_func_idx]

        if len(active_q_func.replay_buffer) >= active_q_func.replay_buffer.config.batch_size:

            if n_iterations is None:
                n_iterations = self.config.q_func.n_iterations_per_training

            for cur_iter in range(n_iterations):

                # get batch
                sampled_data = active_q_func.replay_buffer.sample_and_split_as_tensors()
                state_batch, action_batch, next_state_batch, reward_batch, not_done_batch = sampled_data

                # calculate current state q-values
                tmp = self._squeeze_state_action_tensor_if_necessary(
                    active_q_func.model(state_batch)
                )
                cur_q_batch = tmp.gather(1, action_batch)

                next_q_batch = torch.zeros(cur_q_batch.shape[0], device=cur_q_batch.device)

                if torch.any(not_done_batch):
                    with torch.no_grad():
                        tmp = self._squeeze_state_action_tensor_if_necessary(
                            active_q_func.model(next_state_batch[not_done_batch])
                        )
                        next_q_batch[not_done_batch] = tmp.max(1)[0].detach()

                # Compute the expected Q values
                expected_state_action_values = reward_batch + (self.config.gamma * next_q_batch)

                # calc loss
                loss = active_q_func.loss_func(cur_q_batch, expected_state_action_values.unsqueeze(1))

                # backward pass
                active_q_func.optimizer.zero_grad()
                loss.backward()

                # avoid exploding weights and gradients using gradient clipping
                if self.config.q_func.gradient_clipping_value is not None:
                    torch.nn.utils.clip_grad_value_(active_q_func.model.parameters(), clip_value=self.config.q_func.gradient_clipping_value)

                # optimize the model
                active_q_func.optimizer.step()


    def calc_q(self, state, action=None, reward_func_idx=None):

        if reward_func_idx is None:
            reward_func_idx = self.active_reward_func_idx

        if np.ndim(state) == 1:
            state_tensor = torch.tensor([state], dtype=torch.float)
            if action is not None:
                action_tensor = torch.tensor([action], dtype=torch.long)
        else:
            state_tensor = torch.tensor(state, dtype=torch.float)
            if action is not None:
                action_tensor = torch.tensor(action, dtype=torch.long)

        with torch.no_grad():
            out = self._squeeze_state_action_tensor_if_necessary(
                self.q_func_per_reward_func[reward_func_idx].model(state_tensor)
            )
            if action is not None:
                if action_tensor.ndim == 1:
                    action_tensor = torch.unsqueeze(action_tensor, 1)
                out = out.gather(1, action_tensor)

        out = out.numpy()

        if np.ndim(state) == 1:
            out = out[0]

        if action is not None:
            out = out[0]

        return out


    def calc_expected_return(self, state, action=None, reward_func_idx=None):
        return self.calc_q(state, action, reward_func_idx=reward_func_idx)


    @staticmethod
    def _squeeze_state_action_tensor_if_necessary(state_action_tensor):
        """
        Some models allow to encode vectors, thus they return the action value in a separate dimension.
        This function removes this dimension, if such a model is used.
        """
        if state_action_tensor.ndim == 3 and state_action_tensor.shape[2] == 1:
            state_action_tensor = torch.squeeze(state_action_tensor, 2)
        return state_action_tensor

