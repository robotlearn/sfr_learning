##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
import numpy as np
import exputils as eu
import warnings
import gym


class QLearningNumpy:
    """
    Q-learning
    """

    @staticmethod
    def default_config():
        dc = eu.AttrDict(
            alpha = 0.05,
            epsilon = 0.15, # can also be a dict that defines the function to get epsilon
            gamma = 0.95,
            z_init_std = 0.01,
            z_init = None,
            absolute_weight_maximum = 1000,  # maximum absolute value of weights used for w and z
        )
        return dc
        

    def __init__(self, env, config=None, **kwargs):
        self.config = eu.combine_dicts(kwargs, config, self.default_config())

        self.alpha = self.config.alpha
        self.epsilon = self.config.epsilon

        self.z_per_reward_func = []

        self.active_reward_func_idx = None

        # obs, feature and action space
        if not isinstance(env.observation_space, gym.spaces.Dict) and 'observation' in env.observation_space.spaces and 'feature' in env.observation_space.spaces:
            raise ValueError('Only Dict observation spaces are support that have a \'observation\' and \'feature\' attribute!')

        self.observation_space = env.observation_space.spaces['observation']
        self.action_space = env.action_space


    def add_reward_function(self, reward_function, reward_func_descr=None):
        # set up q-function for the new reward function
        z = np.random.randn(self.action_space.n, self.observation_space.shape[0]) * self.config.z_init_std
        self.z_per_reward_func.append(z)
        return len(self.z_per_reward_func) - 1


    def set_active_reward_func_idx(self, idx):
        self.active_reward_func_idx = idx


    def get_action(self, obs, info, exp_info):
        """Draws an epsilon-greedy policy"""

        obs = obs['observation']

        action = self.calc_max_action(obs)
        if np.random.rand() < self.epsilon:
            action = np.random.choice(np.arange(self.action_space.n))

        return action


    def update(self, transition, exp_info):
        """Use the given transition to update the agent."""

        # extract transition information
        obs, action, reward, next_obs, done, _ = transition
        obs = obs['observation']
        next_obs = next_obs['observation']

        if done:
            gamma = 0
            next_action = 0  # just a dummy action
        else:
            gamma = self.config.gamma
            next_action = self.calc_max_action(next_obs, target_reward_func_idx=self.active_reward_func_idx)

        # update Q function
        z = self.z_per_reward_func[self.active_reward_func_idx]
        current_q = np.matmul(obs, z[action, :])
        next_q = np.matmul(next_obs, z[next_action, :])
        z[action, :] += self.alpha * (reward + gamma * next_q - current_q) * obs
        self.enforce_weight_maximum(z)


    def calc_max_action(self, state, target_reward_func_idx=None):
        """
        Get the optimal action for a given reward function.
        """

        if target_reward_func_idx is None:
            target_reward_func_idx = self.active_reward_func_idx

        # calculate the Q-values for all actions
        # Q = phi * z * w
        q_values = np.matmul(state, self.z_per_reward_func[target_reward_func_idx].transpose())

        # identify the optimal action
        # select action with highest return over all policies
        max_value = np.max(q_values)
        where_max_value = np.where(q_values == max_value)[0]
        if len(where_max_value) == 1:
            max_action = where_max_value[0]
        else:
            max_action = np.random.choice(where_max_value)

        return max_action


    def calc_expected_return(self, state, action=None, reward_func_idx=None):

        if reward_func_idx is None:
            reward_func_idx = self.active_reward_func_idx

        if np.ndim(state) == 1:
            states = [state]
        else:
            states = state

        if action is not None:
            check_actions = [action]
        else:
            check_actions = [a for a in range(self.action_space.n)]

        q_values = np.zeros((len(states), len(check_actions)))
        for a_idx, a in enumerate(check_actions):
            q_values[:, a_idx] = np.matmul(states, self.z_per_reward_func[reward_func_idx][a, :])

        if action is not None:
            q_values = q_values[:, 0]

        if np.ndim(state) == 1:
            q_values = q_values[0]

        return q_values


    def enforce_weight_maximum(self, weights):

        if self.config.absolute_weight_maximum is not None:

            weight_above_maximum_inds = weights > self.config.absolute_weight_maximum
            if np.any(weight_above_maximum_inds):
                weights[weight_above_maximum_inds] = self.config.absolute_weight_maximum
                warnings.warn('Some weights have reached the maximum value of {}.'.format(self.config.absolute_weight_maximum))

            weight_above_maximum_inds = weights < -1 * self.config.absolute_weight_maximum
            if np.any(weight_above_maximum_inds):
                weights[weight_above_maximum_inds] = -1 * self.config.absolute_weight_maximum
                warnings.warn('Some weights have reached the maximum value of {}.'.format(-1 * self.config.absolute_weight_maximum))
