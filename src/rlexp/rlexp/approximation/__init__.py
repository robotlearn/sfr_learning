##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
from rlexp.approximation.replay_buffer import ReplayBuffer

from rlexp.approximation.linear_nn import LinearNN
from rlexp.approximation.action_separated_linear_nn import ActionSeparatedLinearNN
from rlexp.approximation.action_and_feature_component_separated_linear_nn import ActionAndFeatureComponentSeparatedLinearNN
from rlexp.approximation.action_and_output_separated_linear_nn import ActionAndOutputSeparatedLinearNN
from rlexp.approximation.action_and_feature_component_separated_gamma_normalized_linear_nn import ActionAndFeatureComponentSeparatedGammaNormalizedLinearNN
from rlexp.approximation.rbf_layer import RBFLayer

from rlexp.approximation.custom_factor_mse_loss import CustomFactorMSELoss

from rlexp.approximation.multi_stream_nn import MultiStreamNN
from rlexp.approximation.separated_multi_stream_nn import SeparatedMultiStreamNN

import rlexp.approximation.mdn