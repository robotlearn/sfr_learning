##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
import exputils as eu
import torch
import rlexp
import numpy as np

class SoftmaxStateActionDiscreteEventProbabilityApproximator:

    @staticmethod
    def default_config():
        dc = eu.AttrDict(

            model=eu.AttrDict(
                cls=ActionSeparatedLinearDQN,
            ),

            optimizer = eu.AttrDict(
                cls=torch.optim.SGD,
            ),

            loss_func = eu.AttrDict(
                cls=torch.nn.CrossEntropyLoss,
            ),

            replay_buffer = eu.AttrDict(
                cls=rlexp.approximation.ReplayBuffer,
            ),

            n_iterations = 1,
        )
        return dc


    def __init__(self, state_dim, n_actions, n_events, config=None, **argv):
        self.config = eu.combine_dicts(argv, config, self.default_config())

        self.model = eu.misc.call_function_from_config(
            self.config.model,
            state_dim,
            n_actions,
            n_events,
            func_attribute_name='cls'
        )

        self.optimizer = eu.misc.call_function_from_config(
            self.config.optimizer,
            self.model.parameters(),
            func_attribute_name='cls'
        )

        self.loss_func = eu.misc.call_function_from_config(
            self.config.loss_func,
            func_attribute_name='cls'
        )

        self.replay_buffer = eu.misc.call_function_from_config(
            self.config.replay_buffer,
            func_attribute_name='cls'
        )


    def add_data(self, state, action, event):
        '''Adds new data to the approximator.'''
        data = [torch.Tensor([state]),
                torch.tensor([action], dtype=torch.long),
                torch.tensor([event], dtype=torch.long)]
        self.replay_buffer.add(data)


    def train(self, n_iterations=None):
        '''
        Trains the approximator for n_iterations (either specified as attribute or in config) from samples
        '''

        if len(self.replay_buffer) >= self.replay_buffer.config.batch_size:

            if n_iterations is None:
                n_iterations = self.config.n_iterations

            for cur_iter in range(n_iterations):

                # get batch
                raw_batch_data = self.replay_buffer.sample()

                # get the elements from the batch
                state_batch = []
                action_batch = []
                event_batch = []
                for raw_data in raw_batch_data:
                    state_batch.append(raw_data[0])
                    action_batch.append(raw_data[1])
                    event_batch.append(raw_data[2])

                # create tensors from data
                state_batch = torch.cat(state_batch)
                action_batch = torch.cat(action_batch)
                event_batch = torch.cat(event_batch)

                # forward pass
                out_tensor = self.model(state_batch, action_batch)

                # calc loss
                loss = self.loss_func(out_tensor, event_batch)

                # backward pass
                self.model.zero_grad()
                loss.backward()

                # optimize the model
                self.optimizer.step()


    def calc(self, state, action):

        if np.ndim(state) == 1:
            state_tensor = torch.tensor([state], dtype=torch.float)
            action_tensor = torch.tensor([action], dtype=torch.long)
        else:
            state_tensor = torch.tensor(state, dtype=torch.float)
            action_tensor = torch.tensor(action, dtype=torch.long)

        with torch.no_grad():
            out = self.model(state_tensor, action_tensor)
            out = torch.nn.functional.softmax(out, dim=1)

        out = out.numpy()

        if np.ndim(state) == 1:
            out = out[0]

        return out