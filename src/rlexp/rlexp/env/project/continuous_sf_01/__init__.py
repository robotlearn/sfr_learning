##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
from rlexp.env.project.continuous_sf_01.twod_infinite_track_racer_env import TwoDInfiniteTrackRacerEnv
from rlexp.env.project.continuous_sf_01.rbf_state_twod_infinite_track_racer_env import RBFStateTwoDInfiniteTrackRacerEnv
