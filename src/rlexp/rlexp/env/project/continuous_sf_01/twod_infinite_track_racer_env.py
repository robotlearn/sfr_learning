##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
import numpy as np
import gym
import exputils as eu
import rlexp
import matplotlib.pyplot as plt


def sample_component_independent_rfunc(n_markers, config=None, **kwargs):

    default_config = eu.AttrDict(
        n_possible_gauss_components=[1, 2],
        mu_range=[0.0, 0.7],
        sigma_range=[0.001, 0.01],
        weight_range=[1.0, 1.0],
    )
    config = eu.combine_dicts(kwargs, config, default_config)

    def sample_range(range_def):
        return range_def[0] + (range_def[1] - range_def[0]) * np.random.rand()

    gauss_template = 'np.exp(-(<VALUE> - {:.3f})**2 / {:.5f})'  # placeholders: mu, sigma

    component_functions = []
    component_strings = []

    # marker distance features
    for feature_dim in range(n_markers):

        n_gauss_components = np.random.choice(config.n_possible_gauss_components)

        weight = sample_range(config.weight_range) / n_markers

        component_str = '{:.3f} * np.max(['.format(weight)

        for gauss_component_idx in range(n_gauss_components):

            component_str += gauss_template.format(
                sample_range(config.mu_range),
                sample_range(config.sigma_range)
            )

            if gauss_component_idx < n_gauss_components - 1:
                component_str += ', '

        component_str += '])'

        component_functions.append(eval('lambda feature_val: ' + component_str.replace('<VALUE>', 'feature_val')))
        component_strings.append(component_str.replace('<VALUE>', 'features[{}]'.format(feature_dim)))

    # create final reward function from components
    lambda_str = 'lambda features: ' + ' + '.join(component_strings)
    r_func = eval(lambda_str)

    return r_func, lambda_str, component_functions


class TwoDInfiniteTrackRacerEnv(gym.Env):
    """
        features: Normalized distance [0, 1] to each marker.
    """

    @staticmethod
    def default_config():
        dc = eu.AttrDict()

        # [[min_x, max_x], [min_y, max_y]]
        dc.area = [[0., 1.],
                   [0., 1.]]

        # either a point [x, y, angle] or an area [[min_x, max_x], [min_y, max_y], [min+_angle, max_angle]] in which a point is uniformly sampled
        dc.start_state = np.array([
            [0.1, 0.9],  # x
            [0.1, 0.9],  # y
            [-np.pi, np.pi]  # angle)
        ])

        # location of each marker with [x, y] position
        dc.markers = np.array([
            [0.25, 0.75],
            [0.75, 0.25],
            [0.75, 0.6],
        ])

        # number of max timesteps before an episode ends
        dc.n_max_time_steps = np.inf

        # list of actions with (direction in angles from previous direction, distance)
        dc.actions = [
            (np.pi / 7, 0.06),
            (0.0, 0.075),
            (-np.pi / 7, 0.06),
        ]

        # defines if the env is deterministic (stochasticity=None) or stochastic (position + standard normal error with stochasticity=[sigma_x, sigma_y, sigma_angle])
        dc.stochasticity = (0.005, 0.005, 0.005)

        # defines a possible stochasticity over the features with gaussian noise with a sigma that is defined here
        dc.feature_stochasticity_sigma = 0.0

        # active reward function
        dc.reward_function = lambda feature_array: np.sum(feature_array)

        return dc


    def __init__(self, config=None, **kwargs):
        self.config = eu.combine_dicts(kwargs, config, self.default_config())

        area_x_min = self.config.area[0][0]
        area_x_max = self.config.area[0][1]
        area_y_min = self.config.area[1][0]
        area_y_max = self.config.area[1][1]

        max_possible_marker_distance = np.sqrt((area_x_max - area_x_min)**2 + (area_y_max - area_y_min)**2) / 2.0
        self.marker_distance_normalization_coeff = 1.0 / max_possible_marker_distance

        self.observation_space = gym.spaces.Dict(
            observation=gym.spaces.Box(
                low=np.array([area_x_min, area_x_max, -np.pi], dtype=np.float32),
                high=np.array([area_y_min, area_y_max, np.pi], dtype=np.float32)
            ),
            feature=gym.spaces.Box(
                low=np.array([0.0] * (self.config.markers.shape[0]), dtype=np.float32),
                high=np.array([1.0] * (self.config.markers.shape[0]), dtype=np.float32)
            ),
        )
        self.action_space = gym.spaces.Discrete(len(self.config.actions))

        self.reward_function = self.config.reward_function

        self.state_pos = None
        self.state_angle = None
        self.n_steps = None


    def get_obs(self, agent_state=None, is_feature_stochasticity=True):

        if agent_state is None:
            agent_pos = self.state_pos
            agent_angle = self.state_angle
        else:
            agent_pos = agent_state[:2]
            agent_angle = agent_state[2]

        feature = self.calc_distance_to_markers(agent_pos)

        if is_feature_stochasticity and 'feature_stochasticity_sigma' in self.config and self.config.feature_stochasticity_sigma > 0.0:
            feature += np.random.randn(len(feature)) * self.config.feature_stochasticity_sigma
            # features are in [0 1]
            feature = np.clip(feature, a_min=0.0, a_max=1.0)

        return dict(
            observation=np.array([agent_pos[0], agent_pos[1], agent_angle]),
            feature=feature
        )


    def calc_distance_to_markers(self, position):
        marker_distances = self.calc_min_distance(position, self.config.markers)
        return marker_distances * self.marker_distance_normalization_coeff


    def reset(self):

        # all object exists
        if np.ndim(self.config.start_state) == 1:
            self.state_pos = self.config.start_state[:2]
            self.state_angle = self.config.start_state[2]
        else:
            # [[min_x, max_x, min_angle], [min_y, max_y, max_angle]] in which a point is uniformly sampled
            def sample_in_range(min_val, max_val):
                return min_val + (max_val - min_val) * np.random.rand()

            # sample x
            min_x = self.config.start_state[0][0]
            max_x = self.config.start_state[0][1]
            x = sample_in_range(min_x, max_x)

            min_y = self.config.start_state[1][0]
            max_y = self.config.start_state[1][1]
            y = sample_in_range(min_y, max_y)

            min_direc = self.config.start_state[2][0]
            max_direc = self.config.start_state[2][1]
            direc = sample_in_range(min_direc, max_direc)

            self.state_pos = np.array([x, y], dtype=np.float32)
            self.state_angle = direc

        self.n_steps = 0

        return self.get_obs()


    def step(self, action):

        self.n_steps += 1
        if self.n_steps >= self.config.n_max_time_steps:
            done = True
        else:
            done = False

        action_angle, action_distance = self.config.actions[action]

        delta_x = 0.0
        delta_y = 0.0
        delta_dir = 0.0

        if self.config.stochasticity is not None:
            random_numbers = np.random.randn(len(self.config.stochasticity))

            delta_x = random_numbers[0] * self.config.stochasticity[0]
            delta_y = random_numbers[1] * self.config.stochasticity[1]

            if len(random_numbers) > 2:
                delta_dir = random_numbers[2] * self.config.stochasticity[2]

        # allow angle only to be between -pi and pi
        new_angle = rlexp.utils.keep_value_in_circular_range(self.state_angle + action_angle + delta_dir, -np.pi, np.pi)

        # get new position
        new_x = self.state_pos[0] + np.cos(new_angle) * action_distance + delta_x
        new_y = self.state_pos[1] + np.sin(new_angle) * action_distance + delta_y

        # check if the new position is inside the area
        # otherwise let agent reappear on the other side
        x_min, x_max = self.config.area[0]
        if new_x < x_min:
            new_x = x_max - (x_min - new_x)
        elif new_x > x_max:
            new_x = x_min + (new_x - x_max)

        y_min, y_max = self.config.area[1]
        if new_y < y_min:
            new_y = y_max - (y_min - new_y)
        elif new_y > y_max:
            new_y = y_min + (new_y - y_max)

        self.state_pos[:] = [new_x, new_y]
        self.state_angle = new_angle

        obs = self.get_obs()
        reward = self.reward_function(obs['feature'])
        info = eu.AttrDict(
            position = [new_x, new_y],
            direction = new_angle
        )

        return obs, reward, done, info


    def calc_deterministic_step(self, state, action, step=0):
        """Calculates a deterministic step, providing a model of the environment.

        Args:
            state (list): Position (state[0], state[1]) and angle (state[2])
        """

        state_pos = state[0:2]
        state_angle = state[2]

        if step + 1 >= self.config.n_max_time_steps:
            done = True
        else:
            done = False

        action_angle, action_distance = self.config.actions[action]

        delta_x = 0.0
        delta_y = 0.0
        delta_dir = 0.0

        # allow angle only to be between -pi and pi
        new_angle = rlexp.utils.keep_value_in_circular_range(state_angle + action_angle + delta_dir, -np.pi, np.pi)

        # get new position
        new_x = state_pos[0] + np.cos(new_angle) * action_distance + delta_x
        new_y = state_pos[1] + np.sin(new_angle) * action_distance + delta_y

        # check if the new position is inside the area
        # otherwise let agent reappear on the other side
        x_min, x_max = self.config.area[0]
        if new_x < x_min:
            new_x = x_max - (x_min - new_x)
        elif new_x > x_max:
            new_x = x_min + (new_x - x_max)

        y_min, y_max = self.config.area[1]
        if new_y < y_min:
            new_y = y_max - (y_min - new_y)
        elif new_y > y_max:
            new_y = y_min + (new_y - y_max)

        new_state = np.zeros(3)
        new_state[0:2] = [new_x, new_y]
        new_state[2] = new_angle

        obs = self.get_obs(new_state, is_feature_stochasticity=False)
        reward = self.reward_function(obs['feature'])

        return new_state, obs, reward, done



    def render(self, mode='human', close=False):

        marker_colors = ['green', 'blue', 'red', 'yellow']

        is_calc_reward_map = False
        if not hasattr(self, 'renderer_figure'):
            self.renderer_figure, self.renderer_axis = plt.subplots()
            self.renderer_colorbar = None
            is_calc_reward_map = True
        else:
            self.renderer_axis.clear()

            if self.render_rendered_reward_function is not self.reward_function:
                is_calc_reward_map = True

        if is_calc_reward_map:
            # calculate the map to render the reward function
            self.render_rendered_reward_function = self.reward_function
            self.render_reward_map, self.render_x_map, self.render_y_map = self.calc_reward_map()

            if self.renderer_colorbar:
                self.renderer_colorbar.remove()
                self.renderer_colorbar = None

        # plot the reward map
        reward_plot = self.renderer_axis.pcolor(self.render_x_map, self.render_y_map, self.render_reward_map, shading='auto', cmap="Greys")
        if not self.renderer_colorbar:
            self.renderer_colorbar = plt.colorbar(reward_plot)

        # plot markers
        for marker_idx, marker in enumerate(self.config.markers):
            marker_plot_obj = plt.Circle(marker, radius=0.01, color=marker_colors[marker_idx])
            self.renderer_axis.add_artist(marker_plot_obj)

        # plot the agent position
        agent_circle = plt.Circle(self.state_pos, radius=0.02, color='orange', alpha=0.7)
        self.renderer_axis.add_artist(agent_circle)

        # display plot
        plt.draw()
        plt.pause(0.01)


    def calc_reward_map(self, n_points_x=100, n_points_y=100):

        x_points = np.linspace(self.config.area[0][0], self.config.area[0][1], n_points_x)
        y_points = np.linspace(self.config.area[1][0], self.config.area[1][1], n_points_y)

        x_map, y_map = np.meshgrid(x_points, y_points)

        reward_map = np.empty_like(x_map)
        for i in range(x_map.shape[0]):
            for j in range(y_map.shape[0]):
                obs = self.get_obs(agent_state=[x_map[i, j], y_map[i, j], 0.0], is_feature_stochasticity=False)
                #marker_distances = self.calc_distance_to_markers(np.array([x_map[i, j], y_map[i, j]]))
                reward_map[i, j] = self.reward_function(obs['feature'])

        return reward_map, x_map, y_map


    def calc_min_distance(self, point1, point2):

        diff_x, diff_y = self.calc_min_difference(point1, point2)

        dist = (diff_x ** 2 + diff_y ** 2) ** (1. / 2)

        return dist


    def calc_min_difference(self, point1, point2):

        point1 = np.array(point1)
        point2 = np.array(point2)

        x_min, x_max = self.config.area[0]
        y_min, y_max = self.config.area[1]

        max_x_diff = (x_max - x_min) / 2.
        max_y_diff = (y_max - y_min) / 2.

        if point1.ndim == 1:
            point1 = np.array([point1])
            single_point = True
        else:
            single_point = False

        if point2.ndim == 1:
            point2 = np.array([point2])
        else:
            single_point = False

        diff_x = np.abs(point1[:, 0] - point2[:, 0])
        diff_y = np.abs(point1[:, 1] - point2[:, 1])

        diff_x_case = diff_x > max_x_diff
        diff_y_case = diff_y > max_y_diff

        diff_x[diff_x_case] = 2 * max_x_diff - diff_x[diff_x_case]
        diff_y[diff_y_case] = 2 * max_y_diff - diff_y[diff_y_case]

        if single_point:
            diff_x = diff_x[0]
            diff_y = diff_y[0]

        return diff_x, diff_y


    def plot_env(self, axis=None, plot_colorbar=True, cmap=plt.cm.coolwarm, vmin=None, vmax=None, n_points_per_dim=100):

        if axis is None:
            is_axis_given = False
            figure, axis = plt.subplots()
        else:
            is_axis_given = True

        marker_colors = ['green', 'blue', 'red', 'yellow']

        # calculate the map to render the reward function
        reward_map, x_map, y_map = self.calc_reward_map( n_points_x=n_points_per_dim, n_points_y=n_points_per_dim)

        # plot the reward map
        reward_plot = axis.pcolor(x_map, y_map, reward_map, cmap=cmap, shading='auto', vmin=vmin, vmax=vmax)
        if plot_colorbar:
            plt.colorbar(reward_plot, ax=axis)

        # plot markers
        for marker_idx, marker in enumerate(self.config.markers):
            marker_plot_obj = plt.Circle(marker, radius=0.01, color=marker_colors[marker_idx])
            axis.add_artist(marker_plot_obj)

        if not is_axis_given:
            return figure, axis


if __name__ == '__main__':

    # eu.misc.seed(0)
    #
    #
    # env = TwoDInfiniteTrackRacerEnv(feature_stochasticity_sigma=0.1)
    #
    # env.reward_function, _, _ = sample_component_independent_rfunc(len(env.config.markers))
    #
    # obs = env.reset()
    # print(obs)

    #
    # env.render()
    #
    # print(np.random.rand())
    # for step_idx in range(5):
    #     action = env.action_space.sample()
    #     obs = env.step(action)
    #     env.render()
    #
    #     print(obs)
    #
    # env.plot_env()
    # plt.show()

    r_func, lambda_str, component_functions = sample_component_independent_rfunc(3)

    print(component_functions[0](0.1))
    print(component_functions[1](0.1))
    print(component_functions[2](0.1))