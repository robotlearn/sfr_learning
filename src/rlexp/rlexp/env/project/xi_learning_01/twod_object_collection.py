##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
import numpy as np
import gym
import numbers
import exputils as eu
import matplotlib.pyplot as plt

def sample_value(value_descr=None, random_state=None):

    if random_state is None:
        my_random_state = np.random.RandomState()
        my_random_state.set_state(np.random.get_state())
    else:
        my_random_state = random_state

    if value_descr is None:
        val = my_random_state.rand()

    elif isinstance(value_descr, numbers.Number):  # works also for booleans
        val = value_descr

    elif isinstance(value_descr, list) or isinstance(value_descr, np.ndarray):
        val = value_descr[my_random_state.randint(len(value_descr))]

    elif isinstance(value_descr, tuple):

        if value_descr[0] == 'uniform:discrete':
            val = my_random_state.randint(value_descr[1], value_descr[2]+1)

        elif value_descr[0] == 'uniform:continuous':
            val = value_descr[1] + (my_random_state.rand() * (value_descr[2] - value_descr[1]))

        elif value_descr[0] == 'distribution':
            distr = np.cumsum(value_descr[1])
            val = np.where(distr >= my_random_state.rand())[0][0]

        elif value_descr[0] == 'normal_distr':
            val = my_random_state.normal(value_descr[1], value_descr[2])
        else:
            raise ValueError('Unknown sampling distribution: {!r}'.format(value_descr[0]))
    else:
        val = None

    if random_state is None:
        np.random.set_state(my_random_state.get_state())

    return val



class TwoDObjectCollection:
    """
    2D grid object collection task described in Barreto et al. (2018) (https://arxiv.org/pdf/1606.05312.pdf).
    """

    @staticmethod
    def default_config():
        dc = eu.AttrDict()
        dc.seed = None

        # [x - bottom left point, y - bottom left point, x - top right point, y - top right point]
        dc.area = np.array([0.0, 0.0, 1.0, 1.0])

        dc.init_positions = [((0.05, 0.05), 0.0)] # (position, radius)

        dc.goal_positions = np.array([ # (x_position, y_position, radius)
            [0.86, 0.86, 0.1]
            ])

        dc.walls = np.array([ # [x - bottom left point, y - bottom left point, x - top right point, y - top right point]
            [0.00, 0.00, 0.04, 1.00],  # left wall
            [0.00, 0.00, 1.00, 0.04],  # bottom wall
            [0.00, 0.96, 1.00, 1.00],  # top wall
            [0.96, 0.00, 1.00, 1.00],  # right wall

            [0.00, 0.48, 0.16, 0.52],  # left divider
            [0.84, 0.48, 1.00, 0.52],  # right divider
            [0.48, 0.00, 0.52, 0.16],  # bottom divider
            [0.48, 0.84, 0.52, 1.00],  # top divider

            [0.32, 0.48, 0.68, 0.52],  # inner divider wall (left to right)
            [0.48, 0.32, 0.52, 0.68],  # inner divider wall (bottom to top)
            ])

        dc.objects = np.array([  # each object has a [x_pos, y_pos, radius]
            [0.08, 0.56, 0.04],
            [0.08, 0.92, 0.04],
            [0.20, 0.50, 0.04],
            [0.44, 0.56, 0.04],
            [0.44, 0.92, 0.04],
            [0.50, 0.20, 0.04],
            [0.50, 0.80, 0.04],
            [0.56, 0.08, 0.04],
            [0.56, 0.44, 0.04],
            [0.80, 0.50, 0.04],
            [0.92, 0.08, 0.04],
            [0.92, 0.44, 0.04],
            ])

        dc.object_features = np.array([  # [is_type_1, is_type_2, is_type_3]
            [1, 0, 0],
            [0, 1, 0],
            [0, 0, 1],
            [0, 0, 1],
            [1, 0, 0],
            [1, 0, 0],
            [0, 1, 0],
            [0, 0, 1],
            [1, 0, 0],
            [0, 1, 0],
            [0, 1, 0],
            [0, 0, 1],
        ])

        dc.actions = [   # describes for each action the change in each dimension
            (0.0, ('normal_distr', 0.05, 0.005)),   # north
            (('normal_distr', 0.05, 0.005), 0.0),  # east
            (0.0, ('normal_distr', -0.05, 0.005)),   # south
            (('normal_distr', -0.05, 0.005), 0.0),  # west
        ]
        # active reward function
        dc.reward_function = lambda feature_array: np.sum(feature_array)

        return dc


    def __init__(self, config=None, **kwargs):
        self.config = eu.combine_dicts(kwargs, config, self.default_config())

        if len(self.config.init_positions) != 1 or self.config.init_positions[0][1] != 0.0:
            raise NotImplementedError('Only one single point allowed as initial position!')

        self.reward_function = self.config.reward_function

        # create a random state for the environment, so that it is not interfered by other random sampling
        # such as from the agent
        # if not seed is given in the config, then use the state of the current numpy random state
        if self.config.seed:
            self.random_state = np.random.RandomState(self.config.seed)
        else:
            self.random_state = np.random.RandomState()
            self.random_state.set_state(np.random.get_state())

        # is_reached_object_type + is _reached_goal
        if self.config.object_features.size > 0:
            self.n_features = len(self.config.object_features[0]) + 1
        else:
            self.n_features = 1

        # observations: n_visible_people x nearest_people indication x nearest_speaker_indication (0: no_speaker, 1: at_position, 2: left, 3: right)
        self.observation_space = gym.spaces.Dict(
            observation=gym.spaces.Dict(
                position=gym.spaces.Box(low=np.array([self.config.area[0], self.config.area[1]], dtype=np.float32),
                                        high=np.array([self.config.area[2], self.config.area[3]], dtype=np.float32)),
                object=gym.spaces.MultiBinary(len(self.config.objects))
            ),
            feature=gym.spaces.MultiBinary(self.n_features)
        )
        self.action_space = gym.spaces.Discrete(len(self.config.actions))



        self.state_agent_pos = np.array([np.nan, np.nan])
        self.state_agent_hit_object = np.zeros(len(self.config.objects), dtype=np.int8)
        self.state_exist_object = np.ones(len(self.config.objects), dtype=bool)
        self.state_is_at_goal = False


    def set_seed(self, seed):
        self.config.seed = seed
        self.random_state = np.random.RandomState(seed=seed)


    def get_obs(self):

        feature = np.zeros(self.n_features, dtype=bool)

        # combine features of all hit objects using an or operator
        if self.n_features > 1:
            for object_feature in self.config.object_features[self.state_agent_hit_object.astype(bool)]:
                feature[:-1] = np.logical_or(feature[:-1], object_feature)

        # identify if the goal state was hit
        feature[-1] = self.state_is_at_goal

        obs = dict(
            observation=dict(
                position=self.state_agent_pos.copy(),
                object=self.state_agent_hit_object.copy()
            ),
            feature=feature
        )

        return obs


    @staticmethod
    def _detect_collision(pos, objects):

        if objects.size == 0:
            return False

        if objects.ndim == 1:
            is_single_object = True
            objects = np.array([objects])
        else:
            is_single_object = False

        if objects.shape[1] == 3:
            # circles with [x, y, radius]
            is_collision = np.sqrt(np.sum(np.power(objects[:,0:2] - pos, 2), axis=1)) <= objects[:,2]

        elif objects.shape[1] == 4:
            # rectangles with [lower left point - x, lower left point - y, upper right point - x, upper right point - y]
            is_collision = np.logical_and(
                np.logical_and(objects[:, 0] <= pos[0],
                               objects[:, 1] <= pos[1]),
                np.logical_and(objects[:, 2] >= pos[0],
                               objects[:, 3] >= pos[1]))
        else:
            raise ValueError('Unknown object type!')

        if is_single_object:
            is_collision = is_collision[0]

        return is_collision


    def reset(self):

        # all object exists
        self.state_exist_object[:] = 1
        self.state_agent_hit_object[:] = 0
        self.state_is_at_goal = False

        # init position of the agent
        self.state_agent_pos[0] = self.config.init_positions[0][0][0] # x
        self.state_agent_pos[1] = self.config.init_positions[0][0][1] # y

        return self.get_obs()


    def step(self, action):

        if action < 0 or action >= self.action_space.n:
            raise ValueError('Action {!r} is not supported!'.format(action))

        self.state_agent_hit_object[:] = False

        # update the position of the agent
        action_def = self.config.actions[action]
        new_pos = self.state_agent_pos.copy()
        for pos_dim in [0,1]:
            new_pos[pos_dim] += sample_value(action_def[pos_dim], self.random_state)

        # check if the new position is valid, i.e. in the area and not in a wall
        if self._detect_collision(new_pos, self.config.area) and not np.any(self._detect_collision(new_pos, self.config.walls)):
            self.state_agent_pos = new_pos

        # check if an object was hit
        if self.config.objects.size > 0:
            hit_objects = self._detect_collision(self.state_agent_pos, self.config.objects[:, 0:3])
            hit_objects = np.logical_and(self.state_exist_object, hit_objects)
        else:
            hit_objects = np.array([])

        self.state_agent_hit_object[:] = hit_objects[:]

        # existing objects exits in the past and are not hit currently
        self.state_exist_object = np.logical_and(self.state_exist_object, np.logical_not(hit_objects))

        self.state_is_at_goal = np.any(self._detect_collision(self.state_agent_pos, self.config.goal_positions))

        obs = self.get_obs()
        # calculate the reward based on the sampled feature
        reward = self.reward_function(obs['feature'])
        done = self.state_is_at_goal
        info = {}

        return obs, reward, done, info


    def render(self, mode='human', close=False):

        object_colors = ['green', 'blue', 'red']

        if not hasattr(self, 'renderer_figure'):
            self.renderer_figure, self.renderer_axis = plt.subplots()
        else:
            self.renderer_axis.clear()

        # plot objects
        for obj_idx, object in enumerate(self.config.objects):
            alpha = 1.0 if self.state_exist_object[obj_idx] else 0.2
            #obj = plt.Circle(object[0:2], radius=object[2], color='green'object_colors[int(object[3])], alpha=alpha)
            obj = plt.Circle(object[0:2], radius=object[2], color='green', alpha = alpha)
            self.renderer_axis.add_artist(obj)

        # add goal regions
        for goal_descr in self.config.goal_positions:
            goal = plt.Circle(goal_descr[0:2], radius=goal_descr[2], color='yellow')
            self.renderer_axis.add_artist(goal)

        # plot walls
        for wall in self.config.walls:
            cur_wall = plt.Rectangle(xy=wall[0:2],
                                     width=wall[2] - wall[0],
                                     height=wall[3] - wall[1],
                                     color='black')
            self.renderer_axis.add_artist(cur_wall)

        # plot the agent position
        agent_circle = plt.Circle(self.state_agent_pos, radius=0.02, color='orange', alpha=0.7)
        self.renderer_axis.add_artist(agent_circle)

        # display plot
        plt.draw()
        plt.pause(0.01)


def run_env_test():

    env = TwoDObjectCollection()

    env.reset()
    env.render()

    for step_idx in range(300):
        action = env.action_space.sample()
        obs = env.step(action)
        env.render()

        print(obs)

if __name__ == '__main__':
    run_env_test()
