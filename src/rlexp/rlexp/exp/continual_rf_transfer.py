##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
import rlexp
import exputils as eu
import exputils.data.logging as log
import numpy as np
import gym
import warnings

def run_training(config=None, **kwargs):
    """
    Runs RL experiments in a continual transfer setting between tasks with changing reward function.

    Experiment is splitted in different phases. In each phase a different reward function is active for the environment.
    Agents are informed about a change of the phase and the corresponding reward function via:
        > new_rfunc_idx = agent.add_reward_function(rfunc)
        > agent.set_active_reward_func_idx(new_rfunc_idx)
    where rfunc is a handle to a reward function.

    :return:   log, agent, and environment
    """

    ###############
    # configuration

    default_config = eu.AttrDict(
        seed = None,
        env = None,
        agent = None,
        reward_functions = None,
        n_max_steps_per_phase = np.inf,
        n_max_episodes_per_phase = np.inf,
        n_max_steps_per_epsiode = np.inf,
        log_functions = [],
        resume_from_log = None,
        save_log_automatically = False,
        log_agent_after_each_phase = False,
        log_name_episode_per_phase = 'episode_per_phase',
        log_name_step_per_phase = 'step_per_phase',
        log_name_reward_per_phase = 'reward_per_phase',
        log_name_total_reward = 'total_reward',
        log_name_time_per_phase = 'time_per_phase',
        log_name_time = 'time',
    )

    if config == 'get_default_config':
        return default_config

    # use copy mode 'copy' so that agents and environments that ar given are not deep copied
    # this results in a call by reference and not call by value behavior for these elements
    config = eu.combine_dicts(kwargs, config, default_config, copy_mode='copy')

    if config.env is None:
        raise ValueError('Environment must be defined!')

    if config.agent is None:
        raise ValueError('Agent must be defined!')

    if config.reward_functions is None:
        raise ValueError('Reward functions must be defined!')

    if config.n_max_steps_per_phase == np.inf and config.n_max_episodes_per_phase == np.inf:
        raise ValueError('Configuration n_max_steps_per_phase or n_max_episodes_per_phase must not be inf.')

    ###############
    # preparation


    resume_log_obj = None

    # should the training resume from a previous training run
    if config.resume_from_log is not None:

        # if the previous run has not the information needed to continue, a FileNotFoundError will be raised
        # if so, then start fresh
        try:
            if config.resume_from_log is True:
                # try to load the log from current log
                resume_log_obj = log.get_log()
                resume_log_obj.load()
            elif isinstance(config.resume_from_log, str):
                # try to load log from given path
                resume_log_obj = eu.data.logging.Logger(directory=config.resume_from_log)
                resume_log_obj.load()
            else:
                # otherwise, assume given obj is the log
                resume_log_obj = config.resume_from_log

            # set as active log the log from which the training should resume
            log.set_log(resume_log_obj)

            # remove the total time and the total reward from the log, so that they can be reset with the new values
            if log.contains(config.log_name_time):
                log.clear(config.log_name_time)
            if log.contains(config.log_name_total_reward):
                log.clear(config.log_name_total_reward)

            # load needed variables to resume
            env = resume_log_obj.load_single_object('final_env')
            agent = resume_log_obj.load_single_object('final_agent')
            experiment_resume_info = resume_log_obj.load_single_object('experiment_resume_info')
            next_phase_seed = experiment_resume_info['next_phase_seed']
            start_phase = experiment_resume_info['last_phase_idx'] + 1

        except FileNotFoundError:
            warnings.warn('Could not find an existing log from which to resume. Run training from start ...')
            resume_log_obj = None

    if config.seed is not None:
        eu.misc.seed(config.seed)

    if resume_log_obj is None:
        # initialize new training run

        # get env
        if isinstance(config.env, dict):
            if 'cls' in config.env:
                env = eu.misc.call_function_from_config(config.env, func_attribute_name='cls')
            else:
                env = eu.misc.call_function_from_config(config.env)
        elif isinstance(config.env, str):
            env = gym.make(config.env)
        else:
            env = config.env

        # get agent
        if isinstance(config.agent, dict):
            if 'cls' in config.agent:
                agent = eu.misc.call_function_from_config(config.agent, env=env, func_attribute_name='cls')
            else:
                agent = eu.misc.call_function_from_config(config.agent, env=env)
        else:
            agent = config.agent

        # sample a random seed that will used to seed the phase
        # this is done to allow to continue experiments with additional phases
        next_phase_seed = np.random.randint(2**32)

        start_phase = 0

    ###############
    # execution

    # loop over reward_functions (phases)
    phase_idx = start_phase # set this in case we start a finished experiment and it is not set by the for loop
    for phase_idx, rfunc in enumerate(config.reward_functions[start_phase:], start=start_phase):

        # seed the phase
        eu.misc.seed(next_phase_seed)

        if isinstance(rfunc, tuple):
            rfunc_handle = rfunc[0]
            rfunc_descr = rfunc[1]
        else:
            rfunc_handle = rfunc
            rfunc_descr = None

        env.reward_function = rfunc_handle

        if hasattr(agent.__class__, 'add_reward_function'):
            if rfunc_descr is None:
                agent_rfunc_idx = agent.add_reward_function(rfunc_handle)
            else:
                agent_rfunc_idx = agent.add_reward_function(rfunc_handle, reward_func_descr=rfunc_descr)
            agent.set_active_reward_func_idx(agent_rfunc_idx)

        # give phase number to agent
        default_exp_info = eu.AttrDict(phase=phase_idx)

        rlexp.run_training(
            env=env,
            agent=agent,
            n_max_steps=config.n_max_steps_per_phase,
            n_max_episodes=config.n_max_episodes_per_phase,
            n_max_steps_per_epsiode=config.n_max_steps_per_epsiode,
            log_functions=config.log_functions,
            default_exp_info=default_exp_info,
            log_name_episode=config.log_name_episode_per_phase,  # rename some default log poperty names
            log_name_step=config.log_name_step_per_phase,
            log_name_total_reward=config.log_name_reward_per_phase,
            log_name_time=config.log_name_time_per_phase)

        if config.log_agent_after_each_phase:
            log.add_single_object('agent_phase_{}'.format(phase_idx), agent)

        log_phase_counters(config, phase_idx)

        # sample a random seed that will used to seed the phase
        # this is done to allow to continue experiments with more phases
        # and still to be deterministic according to the seed
        next_phase_seed = np.random.randint(2**32)

        # save information to resume training in case of an error
        if config.save_log_automatically:
            log_resume_information(phase_idx, next_phase_seed, agent, env)
            log.save()

    # log total time
    time_per_phases = np.array(log.get_values(config.log_name_time_per_phase))
    time = np.sum(time_per_phases)
    log.add_value(config.log_name_time, time)

    # log total reward
    log_total_reward(config)

    # only need to save it here if it was not already automatically saved after each phase
    if not config.save_log_automatically:
        log_resume_information(phase_idx, next_phase_seed, agent, env)

    # save log if wanted
    if config.save_log_automatically:
        log.save()

    return log.get_log(), agent, env


def log_resume_information(phase_idx, next_phase_seed, agent, env):

    # save important variables which would be needed to resume the run
    log.add_single_object('experiment_resume_info', dict(
        last_phase_idx=phase_idx,
        next_phase_seed=next_phase_seed
    ))
    log.add_single_object('final_agent', agent)
    log.add_single_object('final_env', env)


def log_phase_counters(config, phase_idx):

    # add extra counters
    if log.contains('step_per_phase'):

        # identify how many new steps were made during the phase
        if log.contains('phase_per_step'):
            n_new_steps = len(log.get_values('step_per_phase')) - len(log.get_values('phase_per_step'))
        else:
            n_new_steps = len(log.get_values('step_per_phase'))

        # what was the total final step over all phases
        final_last_step = log.get_values('step')[-1] if log.contains('step') else -1

        # add counters regarding phase and steps
        for step_idx in range(n_new_steps):
            log.add_value('phase_per_step', phase_idx)
            log.add_value('step', final_last_step + 1 + step_idx)

    if log.contains('episode_per_phase'):

        # identify how many new episodes were made during the phase
        if log.contains('phase_per_episode'):
            n_new_episodes = len(log.get_values('episode_per_phase')) - len(log.get_values('phase_per_episode'))
        else:
            n_new_episodes = len(log.get_values('episode_per_phase'))

        # what was the total final episode over all phases
        final_last_episode = log.get_values('episode')[-1] if log.contains('episode') else -1

        # add counters regarding phase and episodes
        for episode_idx in range(n_new_episodes):
            log.add_value('phase_per_episode', phase_idx)
            log.add_value('episode', final_last_episode + 1 + episode_idx)


def log_total_reward(config):
    log.add_value(
        config.log_name_total_reward,
        np.sum(log.get_values(config.log_name_reward_per_phase)))