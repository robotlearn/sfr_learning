##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
import numpy as np
import exputils as eu
import exputils.data.logging as log
import gym
import time
from copy import copy
from collections import namedtuple


Transition = namedtuple(
    'Transition',
    'observation, action, reward, next_observation, done, info'
)


def run_training(config=None, **kwargs):
    """
    Runs the RL training for the given environment and agent.

    Call with run_training(config='get_default_config') to retrieve default config.

    :param config: Configuration:
        TODO
    :return: log of the rl training.

    Configuration:
        agent: Agent object or a exputils class caller config dict.
        env: Gym environment object, name of gym environment or a exputils class caller config dict.
        seed: Integer seed used for the training. See exputils.misc.seed fuction. (default=None)
        is_update_agent: Should the agents update function be called after each step. (default=True)
    """

    ###############
    # configuration

    default_config = eu.AttrDict(
        seed = None,
        env = None,
        agent = None,
        n_max_steps = np.inf,
        n_max_episodes = np.inf,
        n_max_steps_per_epsiode = np.inf,
        is_update_agent = True,
        logger = None,
        log_functions=[],
        default_exp_info=eu.AttrDict(),
        log_name_episode = 'episode',
        log_name_step = 'step',
        log_name_step_per_episode = 'step_per_episode',
        log_name_episode_per_step = 'episode_per_step',
        log_name_reward_per_episode = 'reward_per_episode',
        log_name_reward_per_step = 'reward_per_step',
        log_name_total_reward = 'total_reward',
        log_name_time = 'time',
    )

    if config == 'get_default_config':
        return default_config

    # use copy mode 'copy' so that agents and environments that ar given are not deep copied
    # this results in a call by reference and not call by value behavior for these elements
    config = eu.combine_dicts(kwargs, config, default_config, copy_mode='copy')

    if config.env is None:
        raise ValueError('Environment must be defined!')

    if config.agent is None:
        raise ValueError('Agent must be defined!')

    if config.n_max_episodes == np.inf and config.n_max_steps == np.inf:
        raise ValueError('Configuration n_max_episodes or n_max_steps must not be inf.')

    ##############
    # preparation

    mode = 'training'

    if config.seed is not None:
        eu.misc.seed(config.seed)

    # internally log-functions are a list
    log_functions = config.log_functions
    if not isinstance(log_functions, list):
        log_functions = [log_functions]

    # set custom logger if given
    if config.logger is not None:
        original_log = log.get_log()
        log.set_log(config.logger)

    try:
        # get env
        if isinstance(config.env, dict):
            if 'cls' in config.env:
                env = eu.misc.call_function_from_config(config.env, func_attribute_name='cls')
            else:
                env = eu.misc.call_function_from_config(config.env)
        elif isinstance(config.env, str):
            env = gym.make(config.env)
        else:
            env = config.env

        # get agent
        if isinstance(config.agent, dict):
            if 'cls' in config.agent:
                agent = eu.misc.call_function_from_config(config.agent, env=env, func_attribute_name='cls')
            else:
                agent = eu.misc.call_function_from_config(config.agent, env=env)
        else:
            agent = config.agent

        ##############
        # execution

        start_time_in_sec = time.time()

        total_reward = 0
        step = 0
        episode = 0
        while episode < config.n_max_episodes and step < config.n_max_steps:

            reward_per_episode = 0
            step_per_episode = 0

            obs = env.reset()

            # inform agent about new episode, if it has the start_episode method
            if hasattr(agent.__class__, 'start_episode'):

                exp_info = copy(config.default_exp_info)
                exp_info.mode = mode
                exp_info.step = step
                exp_info.episode = episode
                exp_info.step_per_episode = step_per_episode

                agent.start_episode(obs, exp_info)

            # logging
            log.add_value(config.log_name_episode, episode)

            # step over episode
            info = {}
            done = False
            while not done and step_per_episode < config.n_max_steps_per_epsiode and step < config.n_max_steps:

                exp_info = copy(config.default_exp_info)
                exp_info.mode = mode
                exp_info.step = step
                exp_info.episode = episode
                exp_info.step_per_episode = step_per_episode

                action = agent.get_action(obs, info, exp_info)

                prev_obs = obs
                obs, reward, done, info = env.step(action)

                #  give agents attr dicts as info if possible
                if isinstance(info, dict):
                    try:
                        info = eu.AttrDict(info)
                    finally:
                        pass

                transition = Transition(
                    observation=prev_obs,
                    action=action,
                    reward=reward,
                    next_observation=obs,
                    done=done,
                    info=info)

                if config.is_update_agent:
                    agent.update(transition, exp_info)

                reward_per_episode += reward
                total_reward += reward

                # logging
                log.add_value(config.log_name_step, step)
                log.add_value(config.log_name_step_per_episode, step_per_episode)
                log.add_value(config.log_name_reward_per_step, reward)
                log.add_value(config.log_name_episode_per_step, episode)

                # additional logging if exists
                for log_func in log_functions:
                    log_func(
                        log,
                        env=env,
                        agent=agent,
                        step=step,
                        episode=episode,
                        step_per_episode=step_per_episode,
                        transition=transition,
                        exp_info=exp_info
                    )

                step += 1
                step_per_episode += 1

            # inform agent about end of episode, if it has the end_episode method
            if hasattr(agent.__class__, 'end_episode'):
                agent.end_episode(obs, exp_info)

            log.add_value(config.log_name_reward_per_episode, reward_per_episode)

            episode += 1

        # log elapsed time
        end_time_in_sec = time.time()
        elapsed_time = end_time_in_sec - start_time_in_sec
        log.add_value(config.log_name_time, elapsed_time)

        log.add_value(config.log_name_total_reward, total_reward)

    finally:
        # reset to original logger
        this_log = log.get_log()
        if config.logger is not None:
            log.set_log(original_log)

    return this_log, agent, env
