##
## This file is part of the software SFR-Learning-2022: Experiments for the paper "Successor Feature Representation".
##
## Copyright INRIA
## Year 2022
## Contact: chris.reinke@inria.fr
##
## The software SFR-Learning-2022 is provided under GPL-3.0-or-later.
##
from rlexp.utils.spaces import num_flat_space_elements
from rlexp.utils.spaces import nd_to_flat_space_index
from rlexp.utils.spaces import flat_to_nd_space_index
from rlexp.utils.set_values import set_value_by_function
from rlexp.utils.set_values import linear_value_adaptation

import numpy as np
import torch
import random

# correct solution:
def softmax(x):
    """Compute softmax values for each sets of scores in x."""
    # e_x = np.exp(x - np.max(x))
    # return (e_x.transpose() / e_x.transpose().sum(axis=0)).transpose()
    x = np.array(x)

    if x.ndim == 1:
        e_x = np.exp(x - np.max(x))

    elif x.ndim == 2:
        e_x = np.exp(x - np.tile(np.max(x, axis=1), (x.shape[1], 1)).transpose())

    else:
        raise NotImplementedError('Softmax function supports only vectors or matrices with 2 dimensions!')

    return (e_x.transpose() / e_x.transpose().sum(axis=0)).transpose()


def seed(seed=None, is_set_random=True, is_set_numpy=True, is_set_torch=True):
    """
    Sets the random seed for random, numpy and pytorch.

    :param seed: Seed (integer) or configuration dictionary which contains a 'seed' property.
                 If None is given, a seed is chosen via torch.seed().
    :param is_set_random: Should random seed of random be set. (default=True)
    :param is_set_numpy: Should random seed of numpy.random be set. (default=True)
    :param is_set_torch: Should random seed of torch be set. (default=True)
    :return: Seed that was set.
    """

    if seed is None:
        seed = torch.seed()
    elif isinstance(seed, dict):
        seed = seed.get('seed', None)

    if is_set_numpy:
        np.random.seed(seed)

    if is_set_random:
        random.seed(seed)

    if is_set_torch:
        torch.manual_seed(seed)

    return seed


def select_max_value(values):
    """
    Returns the maximum value and its idx in the given numpy array. Selects randomly one of the values if there exist
    several maxima (uses np.random.choice).
    """

    max_value = np.max(values)
    max_value_idxs = np.where(values == max_value)[0]
    if len(max_value_idxs) == 1:
        idx = max_value_idxs[0]
    else:
        idx = np.random.choice(max_value_idxs)

    return max_value, idx


def keep_value_in_circular_range(val, min_val, max_val):
    """
    Keeps a value in a certain range which represents a circle where both ends are connected, for example the degrees on a circle.

    Examples:
        keep_value_in_circular_range(val=10, min_val=-180, max_val=180) --> 10
        keep_value_in_circular_range(val=190, min_val=-180, max_val=180) --> -170
        keep_value_in_circular_range(val=-200, min_val=-180, max_val=180) --> 160

    :param val: Value
    :param min_val: Minimum value of the range.
    :param max_val: Maximum value of the range.
    """

    if min_val <= val and val <= max_val:
        return val
    else:

        range_length = max_val - min_val

        if val < min_val:
            d_min = min_val - val
            overspell = d_min % range_length
            return max_val - overspell

        else:
            # val > max_val
            d_max = val - max_val
            overspell = d_max % range_length
            return min_val + overspell
